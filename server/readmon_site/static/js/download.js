var stored_rows;
var arduino_state = 0;
var restart_server_state = 0;

function init_select_dropdowns() {
    var elems = document.querySelectorAll('select');
    var options = document.querySelectorAll('option');
    var instances = M.FormSelect.init(elems, options);
}

function get_value_from_select_form(name, what = "value") {
    var elem = document.getElementById(name);
    if (elem != null) {
        console.log("ELEM: " + elem);
        if (what == "value") {
            return elem.options[elem.selectedIndex].value;
        }
        return elem.options[elem.selectedIndex].text;
    }
    else {
        return "None"
    }
}

function reload_arduino() {
    var name = "#reload_arduino";
    switch (arduino_state) {
        case 0:
            $(name).text("Yes, I want to reload!")
            $(name).removeClass("lighten-1").addClass("darken-4");
            arduino_state = 1;
            break;
        case 1:
            $(name).text("");
            $(name).append("<div class=\"preloader-wrapper small active\">\n" +
                "    <div class=\"spinner-layer spinner-red-only\">\n" +
                "      <div class=\"circle-clipper left\">\n" +
                "        <div class=\"circle\"></div>\n" +
                "      </div><div class=\"gap-patch\">\n" +
                "        <div class=\"circle\"></div>\n" +
                "      </div><div class=\"circle-clipper right\">\n" +
                "        <div class=\"circle\"></div>\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>\n");
            $(name).removeClass("red").removeClass("darken-4").addClass("grey").addClass("lighten-1");
            arduino_state = 4;
            $.ajax({
                type: "GET",
                url: 'reload_arduino',
                success: function (results) {
                    if (results == "ERROR") {
                        $(name).children().remove();
                        $(name).text("Error - active measurement");
                        arduino_state = 2;
                    }
                    else if (results == "OK") {
                        $(name).children().remove();
                        $(name).text("Arduino Reloaded!");
                        $(name).removeClass("grey").removeClass("lighten-1")
                            .addClass("green").addClass("accent-4");
                        arduino_state = 3;
                        setTimeout(reload_arduino, 5000);
                    }
                    else {
                        $(name).children().remove();
                        $(name).text("Error - active measurement");
                        arduino_state = 2;
                    }

                }
            });
            break;
        case 2:
            $(name).text("Reload Arduino Script");
            $(name).addClass("red").addClass("lighten-1").removeClass("grey").removeClass("lighten-1");
            arduino_state = 0;
            break;

        case 3:
            $(name).removeClass("green").removeClass("accent-4").addClass("red").addClass("lighten-1");
            $(name).text("Reload Arduino Script");
            arduino_state = 0;
            break;

    }
}


function reload_server() {
    var name = "#reload_server";
    switch (restart_server_state) {
        case 0:
            $(name).text("Yes, I want to reload!")
            $(name).removeClass("lighten-1").addClass("darken-4");
            restart_server_state = 1;
            break;
        case 1:
            $(name).text("");
            $(name).append("<div class=\"preloader-wrapper small active\">\n" +
                "    <div class=\"spinner-layer spinner-red-only\">\n" +
                "      <div class=\"circle-clipper left\">\n" +
                "        <div class=\"circle\"></div>\n" +
                "      </div><div class=\"gap-patch\">\n" +
                "        <div class=\"circle\"></div>\n" +
                "      </div><div class=\"circle-clipper right\">\n" +
                "        <div class=\"circle\"></div>\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>\n");
            $(name).removeClass("red").removeClass("darken-4").addClass("grey").addClass("lighten-1");
            restart_server_state = 4;
            $.ajax({
                type: "GET",
                url: 'reload_server',
                success: function (results) {
                    if (results == "ERROR") {
                        $(name).children().remove();
                        $(name).text("Error - active measurement");
                        restart_server_state = 2;
                    }
                    else if (results == "OK") {
                        $(name).children().remove();
                        $(name).text("Server Restarted!");
                        $(name).removeClass("grey").removeClass("lighten-1")
                            .addClass("green").addClass("accent-4");
                        restart_server_state = 3;
                        setTimeout(reload_server, 5000);
                    }
                    else {
                        $(name).children().remove();
                        $(name).text("Error - active measurement");
                        arduino_state = 2;
                    }

                }
            });
            break;
        case 2:
            $(name).text("Restart Server");
            $(name).addClass("red").addClass("lighten-1").removeClass("grey").removeClass("lighten-1");
            restart_server_state = 0;
            break;

        case 3:
            $(name).removeClass("green").removeClass("accent-4").addClass("red").addClass("lighten-1");
            $(name).text("Restart Server");
            restart_server_state = 0;
            break;

    }
}

function load_tags() {
    $.ajax({
        type: 'GET',
        url: 'tags',
        success: function (results) {
            //console.log(results)
            results = JSON.parse(results);
            if (results != "NONE") {
                var name = "#tag"
                var txt = "";
                for (var i in results) {
                    txt += "<option value=\"" + results[i] + "\">" + results[i] + "</option>";
                }
                $(name).append(txt);
                init_select_dropdowns();
            }


        },
        cache: false
    });
}

$(document).ready(function () {
    $(".datepicker-modal").css("color", "#1a237e");
    $(".datepicker-date-display").css("background-color", "#1a237e");
    $(".dropdown-content>li>a").css("color", "#1a237e");
    $('.tooltipped').tooltip();
    $(".modal").modal();
    $("#modaldel").modal();
    load_tags();


});

document.addEventListener('DOMContentLoaded', function () {
    $('.sidenav').sidenav({
        edge: 'right'
    });

    $("#nav_slide_out").on("click", function () {
        M.Sidenav.getInstance($("#slide-out")).open();
    });
    $("#reload_arduino").on("click", function () {
        reload_arduino();
    });

    $("#reload_server").on("click", function () {
        reload_server();
    });
    $('#datefrom').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#timefrom').timepicker({
        twelveHour: false,
        defaultTime: '00:00'
    });
    $('#dateto').datepicker({
        format: 'yyyy-mm-dd'
    })
    $('#timeto').timepicker({
        twelveHour: false,
        defaultTime: '23:59'
    });

    $("#remove-def").on("click", function () {
        var arr = new Array();
        //console.log(stored_rows);
        for (var i in stored_rows) {
            var looking_for_name = "b" + stored_rows[i][0] + "-" + stored_rows[i][1];
            var elem = document.getElementById(looking_for_name);
            if (elem.checked) {
                console.log(looking_for_name + " FOUND");
                arr.push(stored_rows[i]);
            }
        }

        datefrom_val = $("#datefrom").val();
        dateto_val = $("#dateto").val();
        timefrom_val = $("#timefrom").val();
        timeto_val = $("#timeto").val();
        tag_val = $("#tag").val();
        var stringified = JSON.stringify(arr);
        var data = {
            datefrom: datefrom_val,
            dateto: dateto_val,
            timefrom: timefrom_val,
            timeto: timeto_val,
            tag: tag_val,
            board_info: stringified
        };
        console.log(data);

        $.ajax({
            type: "DELETE",
            url: 'downloads',
            data: data,
            dataType: 'json',
            error: function (xhr, error) {
                console.log("XHR: ");
                console.log(xhr);
                console.log("ERROR: " + error);
            },
            success: function (results) {
                console.log("LOADING SETTINGS...");
                console.log(results);
                if (results != "NONE") {
                    console.log("Estoy aqui");
                    var txt = "<a href=\"/download_file2\" download id='link'>";
                    $("#content").append(txt);
                    window.open($("#link").attr('href'), '_blank');
                    //$("#asdfg").click();
                    $("#link").remove();
                }
                else {

                }


            }
        });
        console.log("im here");
    });

    $.ajax({
        type: 'GET',
        url: 'downloads',
        dataType: "json",
        success: function (results) {
            console.log(results);
            if (results != "ERROR") {
                add_options_to_form(results);
            }
            else {
                $("#no_content_modal").modal({
                    dismissible: false
                });
                M.Modal.getInstance($("#no_content_modal")).open();
            }
        },
        cache: false
    });
    $("#send_form").on("click", function () {
        var arr = new Array();
        //console.log(stored_rows);
        for (var i in stored_rows) {
            var looking_for_name = "b" + stored_rows[i][0] + "-" + stored_rows[i][1];
            var elem = document.getElementById(looking_for_name);
            if (elem.checked) {
                console.log(looking_for_name + " FOUND");
                arr.push(stored_rows[i]);
            }
        }

        datefrom_val = $("#datefrom").val();
        dateto_val = $("#dateto").val();
        timefrom_val = $("#timefrom").val();
        timeto_val = $("#timeto").val();
        tag_val = get_value_from_select_form("tag", "value")
        var stringified = JSON.stringify(arr);
        var data = {
            datefrom: datefrom_val,
            dateto: dateto_val,
            timefrom: timefrom_val,
            timeto: timeto_val,
            tag: tag_val,
            board_info: stringified
        };
        console.log(data);

        $.ajax({
            type: "POST",
            url: 'downloads',
            data: data,
            dataType: 'json',
            error: function (xhr, error) {
                console.log("XHR: ");
                console.log(xhr);
                console.log("ERROR: " + error);
            },
            success: function (results) {
                console.log("LOADING SETTINGS...");
                console.log(results);
                if (results != "NONE") {
                    console.log("Estoy aqui");
                    var txt = "<a href=\"/download_file\" download id='link'>";
                    $("#content").append(txt);
                    window.open($("#link").attr('href'), '_blank');
                    //$("#asdfg").click();
                    $("#link").remove();
                }
                else {

                }


            }
        });
        console.log("im here");
    });


});


function add_options_to_form(rows) {
    stored_rows = rows;
    //console.log(stored_rows);
    var name1 = "#options_sensor1";
    var name2 = "#options_sensor2";
    var name3 = "#options_sensor3";
    var name4 = "#options_sensor4";
    var txt1 = "";
    var txt2 = "";
    var txt3 = "";
    var txt4 = "";
    for (var i in  rows) {
        var name_board = "[" + rows[i][2] + "] Con-" + rows[i][0] + "/Ch-" + rows[i][1];
        switch (i % 4) {
            case 0:
                //console.log(1);
                txt1 += "                            <label>\n" +
                    "                                <input type=\"checkbox\" name=\"b" + rows[i][0] + "-" + rows[i][1] + "\" id=\"b" + rows[i][0] + "-" + rows[i][1] + "\"/>\n" +
                    "                                <span>" + name_board + "</span>\n" +
                    "                            </label>";
                break;
            case 1:
                //console.log(2);
                txt2 += "                            <label>\n" +
                    "                                <input type=\"checkbox\" name=\"b" + rows[i][0] + "-" + rows[i][1] + "\" id=\"b" + rows[i][0] + "-" + rows[i][1] + "\"/>\n" +
                    "                                <span>" + name_board + "</span>\n" +
                    "                            </label>";
                break;
            case 2:
                //console.log(3);
                txt3 += "                            <label>\n" +
                    "                                <input type=\"checkbox\" name=\"b" + rows[i][0] + "-" + rows[i][1] + "\" id=\"b" + rows[i][0] + "-" + rows[i][1] + "\"/>\n" +
                    "                                <span>" + name_board + "</span>\n" +
                    "                            </label>";
                break;
            case 3:
                //console.log(4);
                txt4 += "                            <label>\n" +
                    "                                <input type=\"checkbox\" name=\"b" + rows[i][0] + "-" + rows[i][1] + "\" id=\"b" + rows[i][0] + "-" + rows[i][1] + "\"/>\n" +
                    "                                <span>" + name_board + "</span>\n" +
                    "                            </label>";
                break;
        }
        //( board_num, channel, dosimeter_name)


    }

    $(name1).append(txt1);
    $(name2).append(txt2);
    $(name3).append(txt3);
    $(name4).append(txt4);

}
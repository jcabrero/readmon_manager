import parallel_process
import time

def startin():
	global process
	print (process.is_running())
	process.start()
	print(process.is_running)
def endin():
	global process 
	print (process.is_running())
	process.shutdown()
	print(process.is_running())

process = parallel_process.MyProcess()
startin()
endin()


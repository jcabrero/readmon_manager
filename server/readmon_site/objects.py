import hashlib
import calendar
import time
class session_id:
    username = None
    session_id = None
    validity = None
    def __init__(self, username):
        self.username = username
        self.validity = calendar.timegm(time.gmtime()) + 900
        self.session_id = self.gen_session_id(self.validity);

    def gen_session_id(self, time):
        to_hash = "saltsaltsalt%s" % str(time)
        return hashlib.md5(to_hash).hexdigest();

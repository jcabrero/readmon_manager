#################
#               #
# CONFIGURATION #
#               #
#################
##################################
# PATH TO SERVER
path_s = "/root/server/readmon_site/"
# IP ASSIGNED
host_s = "0.0.0.0"
# PORT ASSIGNED]
port_s = 80
# DEBUG MODE
debug_s = True
##################################

def path():
    return path_s
def host():
    return host_s
def port():
    return port_s
def debug():
    return debug_s

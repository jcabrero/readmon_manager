# If you need other math function from math lib import it adding it to the list below.a
from math import exp, log1p


def Nothing(data, a, b):
    return data


def Quadratic(data, a, b):
    return a * (data ** b)


def Linear(data, a, b):
    return a * data + b


def NTC_Conversion(data, a, b):
    ret = -273.15 + a / log1p(data / (b * exp(-a / 298.15)))
    return ret

import json
import collections
import functions
import sensors

max_rows = 8
stored_settings = []
frequency_settings = []
time_periods = []
var_tag = ""
var_lever = True;


def get_configuration(how=0):
    if (how == 0):
        print(len(stored_settings), len(stored_settings) is 0)
        if len(stored_settings) is 0:
            return json.dumps("NONE")
        return json.dumps(stored_settings)
    else:
        return stored_settings


def verify_sensors():
    sens = set(sensors.get_sensor_names())
    names = set([x[2] for x in stored_settings])
    diff = list(names - sens)
    print (diff)
    if len(diff) is 0:
        return True
    return False

def process_file(file):
    """ Generates two different lists of tuples:
    a) With each of the dosimeters per sensor.
    b) With the time configuration per sensor"""
    global stored_settings, frequency_settings
    stored_settings = []
    frequency_settings = []
    count_item = [0] * max_rows
    for index_i, i in enumerate(file.readlines()):
        proc = i.rstrip().split("\t")
        # (user_given_sensor_name,  board_num, dosimeter_name, channel)
        stored_settings += [(proc[0], int(proc[1]), j, index_j) for index_j, j in enumerate(proc[2:]) if (j != "None")]
    print
    if not verify_sensors():
        stored_settings = []
        return -1

    for x in stored_settings:
        count_item[x[1]] += 1
    frequency_settings = [(x[1], x[0], 25, "minutes", count_item[x[1]]) for x in stored_settings]

    frequency_settings = list(set(frequency_settings))
    # (board_num, user_given_sensor_name,  frequency, time)
    frequency_settings.sort(key=lambda x: x[0])
    return 0

    # print(stored_settings)
    # print(frequency_settings)


def update_stored_settings(input_array):
    try:
        global stored_settings, frequency_settings
        # print(input_array)
        count_item = [0] * max_rows
        stored_settings = [(str(x[0]), int(x[1]), str(x[2]), int(x[3])) for x in input_array]
        for x in stored_settings:
            count_item[x[1]] += 1
        frequency_settings = [(x[1], x[0], 25, "minutes", count_item[x[1]]) for x in stored_settings]

        frequency_settings = list(set(frequency_settings))
        # (board_num, user_given_sensor_name,  frequency, time)
        frequency_settings.sort(key=lambda x: x[0])
        return json.dumps("OK")
    except Exception as e:
        # print (e)
        return json.dumps("ERROR")


def get_minimum_time():
    expected_measurement_time = 15 * 1000 * len(stored_settings)
    return expected_measurement_time


def get_sensors_to_modify():
    """Generates the sensors to"""
    return set(x[0] for x in stored_settings)


def get_configuration_for_periods(how=0):
    if len(stored_settings) is 0:
        return json.dumps("NONE")
    if (how == 0):
        max_frecquency = (get_minimum_time(), frequency_settings, tag())
        return json.dumps(max_frecquency)
    elif (how == 1):
        return frequency_settings
    elif (how == 2):
        return dict((x, (y, z, t)) for x, y, z, t, _ in frequency_settings)


def translate_to_ms(num, unit):
    t_sec = 1000
    t_min = 60 * t_sec
    t_hour = 60 * t_min
    t_day = 24 * t_hour

    if (unit == "seconds"):
        return num * t_sec
    elif (unit == "minutes"):
        return num * t_min
    elif (unit == "hours"):
        return num * t_hour
    elif (unit == "days"):
        return num * t_day
    else:
        return "NONE"


def establish_periods():
    """Will generate the entries with the format for the cyclic scheduler."""
    # [(0, 'REM', 7, 1000)]
    # [(0, 'REM', 7, 1000)]
    # time_periods= (board_num, dosimeter_name, channel, frequency * time)
    # stored_settings= (user_given_sensor_name,  board_num, dosimeter_name, channel)
    # frequency_settings= (board_num, user_given_sensor_name,  frequency, time)
    # freq=[board_num: (user_given_sensor_name, frequency, time)]
    global time_periods
    freq = get_configuration_for_periods(2);
    # print (freq)
    if (freq == "NONE"):
        return None;
    # We take items 1 to 3 which are interestings and the fourth which is the item
    time_periods = [x[1:4] + (translate_to_ms(*freq[x[1]][1:]),) for x in stored_settings]


def get_tasks_for_scheduler():
    establish_periods()
    return time_periods


def get_boards_and_channels(how=0):
    if len(stored_settings) is 0:
        return json.dumps("ERROR")
    # ( board_num, channel, dosimeter_name)
    if (how == 1):
        return json.dumps([(x[1], x[3], x[2]) for x in stored_settings])
    else:
        return [(x[1], x[3], x[2]) for x in stored_settings]


def get_available_functions():
    reload(functions)
    a = [x for x in dir(functions) if not x.startswith('__')]
    # print(a)
    return json.dumps(a)


def tag(new_tag=None):
    global var_tag
    if new_tag is not None:
        var_tag = new_tag[0:50] if len(new_tag) > 50 else new_tag
    return var_tag


def lever(new_lever=None):
    global var_lever
    if new_lever is not None:
        var_lever = new_lever
    return var_lever

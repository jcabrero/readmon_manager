import multiprocessing
import time
import _mysql
import random
from datetime import datetime
from MySQLdb.constants import FIELD_TYPE
from sensors import get_sensors, insert_record, get_calibration_for_sensor_by_name_and_interval
import urllib2
import functions
import math


class simple_task_scheduler:

    def __init__(self, mp, sensors, tag):
        self.tag = tag
        self.sensors = sensors
        self.plan = []
        self.duty_cycle = min(i for _, _, _, i in mp)
        self.duty_cycle = self.duty_cycle if self.duty_cycle < 60000.0 else 60000.0
        # [function(x) for x in xs if condition]
        self.mp = [x + (math.floor(x[3] / self.duty_cycle), -1) for x in mp]
        self.duty_cycle = self.duty_cycle / 1000.0;

    def increase(self):
        self.mp[:] = [x[:5] + ((x[5] + 1) % x[4],) for x in self.mp]

    def act_on(self):
        return [index for index, i in enumerate(self.mp) if i[5] == 0]

    def run(self, control):
        start = datetime.now()
        while (not control.is_set()):
            # print(self.mp)
            # print("\n\n\nAWAKE\n\n\n")
            # print(self.mp)
            self.increase()
            # ACT HERE
            self.readout(self.act_on())
            # ACT HERE
            end = datetime.now()
            sleep_time = self.duty_cycle - (end - start).total_seconds()
            if (sleep_time > 0):
                time.sleep(sleep_time)
            start = datetime.now()

    def readout(self, act_on_val):
        l = []
        for i in act_on_val:
            readout_info = self.mp[i]
            sensor = self.sensors[readout_info[1]]
            # 'http://localhost/arduino/0/0_7_20.000000_1.000000_100_1000_10_2000/'
            # (board, dosimeter, vset, iset, tramp, tsett, nsteps, maxtime)
            url = "http://localhost:8000/arduino/0/%d_%d_%f_%f_%f_%f_%f_%f/" \
                  % (
                      readout_info[0], readout_info[2], sensor.vset, sensor.iset, sensor.tramp, sensor.tsett,
                      sensor.nsteps,
                      sensor.maxtime)
            control_loop = True
            count = 0
            while control_loop:
                try:
                    print ("URL: %s" % url)
                    res = urllib2.urlopen(url).read().split(",")
                    corrected_v = self.apply_correction(float(res[0]), readout_info[1])
                    # cal_data, date, board, dosimeter, vmeas, imeas, temp, sensor_name, tag
                    l.append((corrected_v, datetime.now().strftime('%Y-%m-%d %H:%M:%S'), readout_info[0],
                              readout_info[2], res[0],
                              res[1], res[2], readout_info[1], self.tag))
                    # print ("URL: %s" % url)
                    print("RES: %s:%s" % (corrected_v, res))
                    control_loop = False
                except urllib2.HTTPError as err:
                    count += 1
                    if (count == 3):
                        control_loop = False
                    print(err)
                    print("There was an error with your request - TRY %d" % count)
        # End for stmt
        if len(l) > 0:
            insert_record(l)

    def apply_correction(self, voltage, sensor_name):
        try:
            calibration = get_calibration_for_sensor_by_name_and_interval(sensor_name, voltage)
            correction = getattr(functions, calibration[0])
            calibrated = correction(voltage, calibration[1], calibration[2])
            return calibrated
        except Exception as e:
            print (e)
            return voltage


class MyProcess(multiprocessing.Process):

    def __init__(self, process_list=[], tag=None):
        self.exit = multiprocessing.Event()
        self.wait_interval = 1
        self.activated = multiprocessing.Event()
        self.control_event = multiprocessing.Event()
        self.number = 1
        self.process_list = process_list
        self.tag = tag
        super(MyProcess, self).__init__()

    def run(self):
        print("Process init")
        self.activated.set()
        self.control_event.set()
        # p = [(0, 'REM', 7, 1000)]
        if (len(self.process_list) != 0):
            sts = simple_task_scheduler(self.process_list, get_sensors(2), self.tag)
            sts.run(self.exit)
        self.control_event.clear()
        print "You exited!"

    def shutdown(self):
        self.activated.clear()
        print "Shutdown initiated"
        self.exit.set()

    def set_interval(self, num):
        self.wait_interval = num

    def is_running(self):
        return self.activated.is_set()

    def is_background_running(self):
        return self.control_event.is_set()

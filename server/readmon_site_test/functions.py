def Nothing(data, a, b):
    return data

def Quadratic(data, a, b):
    return a * (data ** b)

def Linear(data, a, b):
    return a * data + b
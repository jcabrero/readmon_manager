#!/bin/sh
DIRECTORY=`pwd`
WLANIP=`ifconfig wlan0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'`
ETHIP=`ifconfig eth1| grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'`
execute_command(){
    #ARG1 Command
    #ARG2 value
    #ARG3 Success
    #ARG4 Error
    #echo $*
    xd=`eval $1`
    if [ "$xd" == "$2" ]
    then
        echo "$3"
    else
        echo "$4"
        exit 1
    fi
}


execute_command1(){
    #ARG1 Command
    #ARG2 value
    #ARG3 Success
    #ARG4 Error
    #echo $*
    xd=`eval $1`
    #echo $xd
    if [ "$xd" -gt "$2" ]
    then
        echo "$3"
    else
        echo "$4"
        exit 1
    fi
}

execute_command_success(){
    #ARG1 Command
    #ARG2 value
    #ARG3 Success
    #ARG4 Error
    #echo $*
    xd=`eval $1`
    if [ "$?" == "$2" ]
    then
        echo "$3"
    else
        echo "$4"
        exit 1
    fi
}
execute_command_no_out(){
    #ARG1 Command
    #echo $*
    xd=`eval $1`

}

echo "[$WLANIP-$ETHIP]"
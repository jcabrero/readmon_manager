
import _mysql
from crypto import calculate_session_num
def authenticate_user(username, password):
    try:
        print("authenticating \n\tuser:%s\n\tpassword:%s\n"%(username, password))
        cnx = _mysql.connect('127.0.0.1', 'root', 'arduino', 'RADMON')

        username = _mysql.escape_string(username)
        password = _mysql.escape_string(password)
        str_query="select * from user where (username = '%s' AND password= Password('%s'));" % (username, password)

        cnx.query(str_query)
        res = cnx.store_result()
        if(res.num_rows() >= 1):
            print("PASSWORD CORRECT")
            return True;
        else:
            print("PASSWORD INCORRECT")
            return False;

    except _mysql.Error as err:
        print(err)
        cnx.close()
        return False;

def create_session(response, username):
    #create session for userz
    print("CREATING SESSION")
    response.set_cookie("username", username, max_age=3600)
    verification = str(calculate_session_num(username))
    print(verification)
    response.set_cookie("verification", verification, max_age=3600)

    return;

def remove_session():
    #remove session for user
    response.set_cookie("username", "empty", max_age=1)
    response.set_cookie("verification", "empty", max_age=1)

def verify_session(request, response):
    cookie1 = request.get_cookie("username")
    cookie2 = request.get_cookie("verification")
    print("VERIFY SESSION: %s %s" %(cookie1, cookie2))
    if cookie1 is not None and cookie2 is not None:
        if cookie2 == str(calculate_session_num(cookie1)):
            response.set_cookie("username", cookie1, max_age=3600)
            response.set_cookie("verification", str(calculate_session_num(cookie1)), max_age=3600)
            return True;
    return False;
#authenticate_user("a", "b")
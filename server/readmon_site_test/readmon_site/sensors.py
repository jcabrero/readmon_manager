import _mysql
import json
from MySQLdb.constants import FIELD_TYPE
import random
import time
from datetime import datetime
from math import ceil
from settings import get_boards_and_channels, get_configuration, tag, lever
import functions

# Opening connection.
my_conv = {FIELD_TYPE.LONG: int, FIELD_TYPE.FLOAT: float}
cnx = _mysql.connect('127.0.0.1', 'root', 'arduino', 'RADMON', conv=my_conv)
cnx_insertions = _mysql.connect('127.0.0.1', 'root', 'arduino', 'RADMON', conv=my_conv)


class sensor(object):
    def __init__(self, name, vset, iset, tramp, tsett, nsteps, maxtime, measurement_type):
        self.name = name
        self.vset = vset
        self.iset = iset
        self.tramp = tramp
        self.tsett = tsett
        self.nsteps = nsteps
        self.maxtime = maxtime
        self.measurement_type = measurement_type

    def __str__(self):
        print("[Name: %s|VSET: %f|ISET: %f|RAMP_TIME: %f|SETTLE_TIME: %f|RAMPING_STEPS: %f|MAX_MEASUREMENT_TIME: %f]\n" \
              % (self.name, self.vset, self.iset, self.tramp, self.tsett, self.nsteps, self.maxtime))


def get_sensors(how=0):
    str_query = "select * from sensor;"

    if (how == 0):
        x = query_data(str_query, 1)
        return json.dumps(x)
    elif (how == 1):
        arr = [];
        x = query_data(str_query, 0)
        for i in x:
            arr.append(sensor(*i))
        return arr

    else:
        d = {};
        x = query_data(str_query, 0)
        for i in x:
            d[i[0]] = sensor(*i)
        return d


def transform_date(d):
    'This function transforms the date into a normal date'
    return datetime.strptime(d, '%Y-%m-%d %H:%M:%S')


def generate_query(min_date, max_date, rownum=False, modulus=10):
    'This function generates a query string with the parameters given'
    first = True
    str_query = " WHERE (";
    if (rownum):
        first = False
        str_query += "(rownum %% %d = 1) " % modulus

    if (min_date is not None):
        if (not first):
            str_query += "and "
        first = False
        str_query += "(date > \"%s\") " % (transform_date(min_date))

    if (max_date is not None):
        if (not first):
            str_query += "and "
        first = False
        str_query += "(date < \"%s\")" % (transform_date(max_date))

    str_query += ")"
    return str_query if not first else ""


def get_data(min_date=None, max_date=None, wanted_num_rows=10000):
    if (wanted_num_rows is None):
        wanted_num_rows = 10000

    wanted_num_rows = int(wanted_num_rows)

    count_query = "SELECT COUNT(*) FROM RADMON.readout %s;" % (generate_query(min_date, max_date))

    output = query_data(count_query, 0)

    num_rows = int(output[0][0])
    modulus = int(ceil(float(num_rows) / float(wanted_num_rows)))
    print (modulus, wanted_num_rows, num_rows)

    if (num_rows <= 0):
        return "NONE";

    str_query = None;
    if (modulus == 1):
        str_query = "select date, vmeas, imeas, temp from RADMON.readout%s order by date asc;" % (
            generate_query(min_date, max_date))
    else:
        str_query = """ SELECT * FROM ( 
        SELECT @row := @row +1 AS rownum, date, vmeas, imeas, temp FROM 
        (SELECT @row :=0) r, RADMON.readout %s) ranked 
        %s order by date asc;""" % (
            generate_query(min_date, max_date), generate_query(min_date, max_date, True, modulus))

    x = query_data(str_query, 1)

    print("transforming to JSON")
    return json.dumps(x)


def query_data(str_query, how=0):
    'Performs a query and returns all elements as: [0] Tuples [1] Dictionary'
    try:
        print(str_query)
        cnx.query(str_query)
        res = cnx.store_result()
        print ("RETRIEVED RESULTS: %d" % res.num_rows())
        return res.fetch_row(res.num_rows(), how=how)
    except _mysql.Error as err:
        print(err)


def query_data_v2(str_query, how=0):
    'Performs a query and returns all elements as: [0] Tuples [1] Dictionary'
    try:
        print(str_query)
        cnx.query(str_query)
        res = cnx.store_result()
        if (res.num_rows() == 0):
            return None
        print ("RETRIEVED RESULTS: %d" % res.num_rows())
        return res.fetch_row(res.num_rows(), how=how)
    except _mysql.Error as err:
        print(err)


def query_data_cnx2(str_query, how=0):
    'Performs a query and returns all elements as: [0] Tuples [1] Dictionary'
    try:
        print(str_query)
        cnx_insertions.query(str_query)
        res = cnx_insertions.store_result()
        print ("RETRIEVED RESULTS: %d" % res.num_rows())
        return res.fetch_row(res.num_rows(), how=how)
    except _mysql.Error as err:
        print(err)


def insert_sensor(name, vset, iset, tramp, tsett, nsteps, maxtime, meas_type):
    'inserts a new sensor in the database'
    name, meas_type = escape_args(name, meas_type)
    str_query = "INSERT into sensor(sensor_name, vset, iset, ramp_time, settle_time, ramping_steps, max_measurement_time, measurement_type)\
     values ('%s', %12.6f ,%12.6f, %12.6f, %12.6f, %12.6f, %12.6f, '%s');" % (
        name, vset, iset, tramp, tsett, nsteps, maxtime, meas_type)
    print(str_query)
    cnx.query(str_query)


def escape_args(*args):
    if len(args) == 1:
        return cnx.escape_string(args[0])
    return (cnx.escape_string(i) for i in args)


def update_sensor(old_name, name, vset, iset, tramp, tsett, nsteps, maxtime, measurement_type):
    try:
        # ename = cnx.escape_string(name)
        # eold_name = cnx.escape_string(old_name)
        # evset = cnx.escape_string(vset)
        # eiset = cnx.escape_string(iset)
        # etramp = cnx.escape_string(tramp)
        # etsett = cnx.escape_string(tsett)
        # ensteps = cnx.escape_string(nsteps)
        # emaxtime = cnx.escape_string(maxtime)

        old_name, name, measurement_type = escape_args(old_name, name, measurement_type)

        str_query = "UPDATE RADMON.sensor SET sensor_name = \"%s\", vset = %12.6f, iset = %12.6f, ramp_time = %12.6f, settle_time = %12.6f,\
         ramping_steps = %12.6f, max_measurement_time = %12.6f, measurement_type = \"%s\" WHERE sensor_name = \"%s\";" % (
            name, vset, iset, tramp, tsett, nsteps, maxtime, measurement_type, old_name)
        print(str_query)
        cnx.query(str_query)
        return "OK"
    except _mysql.Error as err:
        print(err)
        return "ERROR"


def delete_sensor(name):
    try:
        name = escape_args(name)
        print (name)
        str_query = "DELETE FROM RADMON.sensor where sensor_name = \"%s\"" % name
        print(str_query)
        cnx.query(str_query)
        return "OK"
    except _mysql.Error as err:
        print(err)
        return "ERROR"


def insert_record(insertions):
    'inserts the readouts in the database.e'

    str_query = "INSERT INTO readout(cal_data, date, board, dosimeter, vmeas, imeas, temp, sensor_name, tag) values %s" % (
        ", ".join(str(e) for e in insertions))
    print (str_query)
    cnx_insertions.query(str_query)


def generate_query_2(min_date, max_date, tag, rownum=False, modulus=10):
    'This function generates a query string with the parameters given'
    first = True
    str_query = "AND (";
    if (rownum):
        first = False
        str_query += "(rownum %% %d = 1) " % modulus

    if (tag is not None):
        if (not first):
            str_query += "and "
        first = False
        str_query += "tag=\"%s\" " % tag

    if (min_date is not None):
        if (not first):
            str_query += "and "
        first = False
        str_query += "(date > \"%s\") " % (transform_date(min_date))

    if (max_date is not None):
        if (not first):
            str_query += "and "
        first = False
        str_query += "(date < \"%s\")" % (transform_date(max_date))

    str_query += ")"
    return str_query if not first else ""


def get_data_2(min_date=None, max_date=None, wanted_num_rows=10000, var_tag=None):
    if (get_configuration(1) == []):
        return "Empty"
    if (wanted_num_rows is None):
        wanted_num_rows = 10000

    wanted_num_rows = int(wanted_num_rows)

    # cat_query = "SELECT board, dosimeter from RADMON.readout group by board, dosimeter order by board, dosimeter asc"
    # board_channel = query_data(cat_query, 0)
    board_channel = get_boards_and_channels()
    return_list = []
    sum = 0
    for i in board_channel:
        count_query = "SELECT COUNT(*) FROM RADMON.readout WHERE board = %d AND dosimeter = %d AND sensor_name=\"%s\" %s" % (
            i[0], i[1], i[2], generate_query_2(min_date, max_date, var_tag))
        # count_query = "SELECT COUNT(*) FROM RADMON.readout %s;"%(generate_query(min_date, max_date))
        output = query_data(count_query, 0)
        num_rows = int(output[0][0])
        sum += num_rows
        modulus = int(ceil(float(num_rows) / float(wanted_num_rows)))
        print (modulus, wanted_num_rows, num_rows)
        if (num_rows <= 0):
            continue
        str_query = None;
        if (modulus == 1):
            str_query = "select date, vmeas, imeas, temp from RADMON.readout WHERE board = %d AND dosimeter = %d\
             AND sensor_name=\"%s\"  %s ORDER BY date ASC" % (
                i[0], i[1], i[2], generate_query_2(min_date, max_date, var_tag))
        else:
            str_query = """ SELECT * FROM ( 
            SELECT @row := @row +1 AS rownum, date, vmeas, imeas, temp FROM 
            (SELECT @row :=0) r, RADMON.readout WHERE board = %d AND dosimeter = %d AND sensor_name=\"%s\" %s) ranked 
            %s ORDER BY date ASC;""" % (
                i[0], i[1], i[2], generate_query_2(min_date, max_date, var_tag),
                generate_query(min_date, max_date, True, modulus))

        x = query_data(str_query, 1)
        return_list.append((i[0], i[1], x))
        print("transforming to JSON")
    if (sum == 0):
        print("RETURN NONE")
        return "NONE"
    return json.dumps(return_list)


def get_data_3(min_date=None, max_date=None, wanted_num_rows=10000, var_tag=None):
    if (get_configuration(1) == []):
        return "Empty"
    if (wanted_num_rows is None):
        wanted_num_rows = 10000

    wanted_num_rows = int(wanted_num_rows)

    # cat_query = "SELECT board, dosimeter from RADMON.readout group by board, dosimeter order by board, dosimeter asc"
    # board_channel = query_data(cat_query, 0)
    board_channel = get_boards_and_channels()
    sensors_dict = get_sensors(how=2)

    return_list = []
    sum = 0
    for i in board_channel:
        count_query = "SELECT COUNT(*) FROM RADMON.readout WHERE board = %d AND dosimeter = %d AND sensor_name=\"%s\" %s" % (
            i[0], i[1], i[2], generate_query_2(min_date, max_date, var_tag))
        # count_query = "SELECT COUNT(*) FROM RADMON.readout %s;"%(generate_query(min_date, max_date))
        output = query_data(count_query, 0)
        num_rows = int(output[0][0])
        sum += num_rows
        modulus = int(ceil(float(num_rows) / float(wanted_num_rows)))
        print (modulus, wanted_num_rows, num_rows)
        if (num_rows <= 0):
            continue
        str_query = None;
        if (modulus == 1):
            str_query = "select date, cal_data from RADMON.readout WHERE board = %d AND dosimeter = %d\
             AND sensor_name=\"%s\"  %s ORDER BY date ASC" % (
                i[0], i[1], i[2], generate_query_2(min_date, max_date, var_tag))
        else:
            str_query = """SELECT * FROM ( 
            SELECT @row := @row +1 AS rownum, date, cal_data FROM 
            (SELECT @row :=0) r, RADMON.readout WHERE board = %d AND dosimeter = %d AND sensor_name=\"%s\" %s) ranked 
            %s ORDER BY date ASC;""" % (
                i[0], i[1], i[2], generate_query_2(min_date, max_date, var_tag),
                generate_query(min_date, max_date, True, modulus))

        x = query_data(str_query, 1)
        return_list.append((i[0], i[1], x, sensors_dict[i[2]].measurement_type))
        print("transforming to JSON")
    if (sum == 0):
        print("RETURN NONE")
        return "NONE"
    return json.dumps(return_list)


def validate_calibrations(calibrations):
    min = 0
    max = 125
    if (calibrations[0][4] != min or calibrations[len(calibrations) - 1][5] != max):
        return False
    for i in range(len(calibrations)):
        if (i is not 0) and (calibrations[i - 1][5] != calibrations[i][4]):
            return False
        if (calibrations[i][4] > max or calibrations[i][5] > max):
            return False
        if (calibrations[i][4] < min or calibrations[i][5] < min):
            return False
        if (calibrations[i][4] == calibrations[i][5]):
            return False
        if (calibrations[i][4] > calibrations[i][5]):
            return False

    return True


def insert_calibrations(sensor_name, calibrations):
    try:
        if (not validate_calibrations(calibrations)):
            return "ERROR"
        sensor_name = escape_args(sensor_name)
        remove_query = "DELETE FROM RADMON.calibration WHERE sensor_name = \"%s\"" % sensor_name
        print(remove_query)
        cnx.query(remove_query)
        str_query = "INSERT INTO calibration\
        (sensor_name, function_type, parameter_a, parameter_b, interval_start, interval_end) values %s;" % (
            ", ".join(str(e) for e in calibrations))
        print(str_query)
        cnx.query(str_query)
        return "OK"
    except _mysql.Error as err:
        print(err)
        return "ERROR"


def get_calibration_for_sensor(name):
    try:
        query = "SELECT function_type, parameter_a, parameter_b, interval_start, interval_end FROM RADMON.calibration\
         WHERE sensor_name = \"%s\" order by interval_start, interval_end" % name
        ret = query_data_cnx2(query)
        return ret

    except _mysql.Error as err:
        print(err)
        return "ERROR"


def get_calibration_for_sensor_by_name_and_interval(name, voltage):
    try:
        query = "SELECT function_type, parameter_a, parameter_b FROM RADMON.calibration\
         WHERE sensor_name = \"%s\" AND %f > interval_start AND  %f < interval_end\
         order by interval_start, interval_end" % (name, voltage, voltage)
        ret = query_data(query)

        return ret[0] if len(ret) is not 0 else ('Default', 1.0, 1.0)
    except _mysql.Error as err:
        print(err)
        return "ERROR"


def generate_download_query(sensor_info_list, min_date, max_date, tag):
    added_where = False
    query_str = ""
    if (len(sensor_info_list) > 0):
        if (not added_where):
            query_str += "WHERE "
            added_where = True
        else:
            query_str += "AND "
        is_first = True
        #
        # l = [[x[i] for x in sensor_info_list] for i in range(len(sensor_info_list[0]))]
        # l = [escape_args(x[i]) for x in l]
        # sensor_info_list = [(x, y, z) for x, y, z in l[0], l[1], l[2]]
        sensor_info_list = [(x[0], x[1], escape_args(x[2])) for x in sensor_info_list]
        print(sensor_info_list)
        for i in sensor_info_list:
            if (not is_first):
                query_str += "OR "
            is_first = False

            query_str += "(board = %d AND dosimeter = %d AND sensor_name = \"%s\") " % (i[0], i[1], i[2])
    if (min_date != None):
        if (not added_where):
            query_str += "WHERE "
            added_where = True
        else:
            query_str += "AND "
            query_str += "(date > \"%s\") " % (transform_date(min_date))

    if (max_date is not None):
        if (not added_where):
            query_str += "WHERE "
            added_where = True
        else:
            query_str += "AND "
        query_str += "(date < \"%s\")" % (transform_date(max_date))

    if (tag is not None):
        if (not added_where):
            query_str += "WHERE "
            added_where = True
        else:
            query_str += "AND "
        query_str += "tag = \"%s\"" % (tag)

    return query_str


def get_data_for_download(sensor_info_list, tag, min_date=None, max_date=None):
    if (get_configuration(1) == []):
        return "Empty"
    query_params = generate_download_query(sensor_info_list, min_date, max_date, tag);
    num_query = "SELECT board, dosimeter, sensor_name, tag, date, cal_data, vmeas, imeas, temp FROM  RADMON.readout %s;" % query_params
    print(num_query)
    cnx.query(num_query)
    res = cnx.store_result()
    total_rows = res.num_rows()
    print(total_rows)
    f = open("temp_files/temp.csv", "w+")
    f.write("BOARD, DOSIMETER, SENSOR NAME, DATE, CALIBRATED DATA, Measured Voltage, Measured Intensity, Temperature\n")
    for i in range(0, total_rows, 1000):
        num_rows = min(1000, total_rows - i)
        result = res.fetch_row(num_rows)
        out = "\n".join(str(x)[1:len(str(x)) - 1] for x in result) + "\n"
        out = out.replace("'", "")
        #print(out)
        f.write(out)

    f.close()
    # cat_query = "SELECT board, dosimeter from RADMON.readout group by board, dosimeter order by board, dosimeter asc"


def remove_data(sensor_info_list, tag, min_date=None, max_date=None):
    if (get_configuration(1) == []):
        return "Empty"
    query_params = generate_download_query(sensor_info_list, min_date, max_date, tag);
    num_query = "SELECT board, dosimeter, sensor_name, tag, date, cal_data, vmeas, imeas, temp FROM  RADMON.readout %s;" % query_params
    print(num_query)
    cnx.query(num_query)
    res = cnx.store_result()
    total_rows = res.num_rows()
    print(total_rows)
    f = open("temp_files/deleted_data.csv", "w+")
    f.write("Board, Dosimeter, Sensor Name, Tag,  Date, Calibrated Data, Measured Voltage, Measured Intensity, Temperature\n")
    for i in range(0, total_rows, 1000):
        num_rows = min(1000, total_rows - i)
        result = res.fetch_row(num_rows)
        out = "\n".join(str(x)[1:len(str(x)) - 1] for x in result) + "\n"
        out = out.replace("'", "")
        #print(out)
        f.write(out)

    f.close()
    num_query = "DELETE FROM RADMON.readout %s;" % query_params
    print(num_query)
    cnx.query(num_query)
    return


def get_tags():
    str_query = "select tag from RADMON.readout GROUP BY tag;";
    output = query_data_v2(str_query, 0)
    if (output is None):
        return json.dumps("NONE")
    return json.dumps(output)

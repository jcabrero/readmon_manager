<h3 align="center">READMON Manager</h3>

```
python hello.py
```

The main directories are:

- html: All the webpages html.
- static/css: Contains all CSS code.
- static/js: Contains the client side functionality.
- static/fonts: Contains all the fonts.
- hello.py: It is the main functionality of the server acting as frontend.
- sensors.py: It is in charge of extracting data from the database.
- parallel_process.py: It represents a parallel functionality that is running in background for data acquisition.
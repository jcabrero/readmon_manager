var chart; // global
var chart1;
var chart2;
var prev;
var state;
var state_live_button = 1;
var live = true;
var timeout = 10000;
var color_index = 0;

function get_two_digits(num) {
    if (num < 10) {
        return "0" + num;
    } else return num;
}

function findIndexByName(name) {
    var find1 = -1, find2 = -1;
    //I know this might be terribly inefficient.
    for (var i in chart1.series) {
        //console.log(name + "==" + chart1.series[i].name);
        if (chart1.series[i].name == name) {
            find1 = i;
        }
    }
    for (var i in chart2.series) {
        //console.log(name + "==" + chart1.series[i].name);
        if (chart2.series[i].name == name) {
            find2 = i;
        }
    }
    return [find1, find2];
}


function init_select_dropdowns() {
    var elems = document.querySelectorAll('select');
    var options = document.querySelectorAll('option');
    var instances = M.FormSelect.init(elems, options);
}

function load_tags() {
    $.ajax({
        type: 'GET',
        url: 'tags',
        success: function (results) {
            //console.log(results)
            results = JSON.parse(results);
            if (results != "NONE") {
                var name = "#tag"
                var txt = "";
                for (var i in results) {
                    txt += "<option value=\"" + results[i] + "\">" + results[i] + "</option>";
                }
                $(name).append(txt);
                init_select_dropdowns();
            }

            console.log("PREV:" + prev);

        },
        cache: false
    });
}

function add_new_series(name, point, redraw, function_to_attach) {

    var data_array1 = new Array();

    for (var it = 0; it < point.length; it++) {
        date_entry = new Date(point[it].date);
        var tuple = [date_entry.getTime(), point[it].cal_data];
        data_array1.push(tuple);
    }
    var prev_dat = new Date(point[point.length - 1].date);
    if (prev == undefined || prev < prev_dat) {
        prev = prev_dat;
    }

    var name1 = name;
    var color = (color_index++) % (Highcharts.getOptions().colors.length);
    console.log(function_to_attach);
    if (function_to_attach == "Dose") {
        console.log("Adding chart1");
        var series1 = chart1.addSeries({
            name: name1,
            showInNavigator: true,
            data: data_array1,
            type: 'spline',
            yAxis: 0,
            threshold: null,
            tooltip: {
                valueDecimals: 2,
                valueSuffix: ' Gy'
            },
            color: Highcharts.getOptions().colors[color],
            fillColor: {
                linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                },
                stops: [
                    [0, Highcharts.getOptions().colors[color]],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[color]).setOpacity(0).get('rgba')]
                ]
            }
        }, false); // add new series, don't redraw

        series1.hide();
    }
    else if (function_to_attach == "Fluence") {
        console.log("Adding chart2");
        var series2 = chart2.addSeries({
            name: name1,
            showInNavigator: true,
            data: data_array1,
            type: 'spline',
            yAxis: 0,
            threshold: null,
            tooltip: {
                valueDecimals: 2,
                valueSuffix: ' n_1MeV / cm_2'
            },
            color: Highcharts.getOptions().colors[color],
            fillColor: {
                linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                },
                stops: [
                    [0, Highcharts.getOptions().colors[color]],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[color]).setOpacity(0).get('rgba')]
                ]
            }
        }, false); // add new series, don't redraw
        series2.hide();
    }
    else {
        console.log("Adding Temperature 1");
        var series1 = chart1.addSeries({
            name: name1,
            showInNavigator: true,
            data: data_array1,
            dashStyle: 'shortdot',
            type: 'spline',
            yAxis: 1,
            threshold: null,
            tooltip: {
                valueDecimals: 2,
                valueSuffix: ' ºC'
            },
            color: Highcharts.getOptions().colors[color],
            fillColor: {
                linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                },
                stops: [
                    [0, Highcharts.getOptions().colors[color]],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[color]).setOpacity(0).get('rgba')]
                ]
            }
        }, false); // add new series, don't redraw
        console.log("Adding Temperature 2");
        var series2 = chart2.addSeries({
            name: name1,
            showInNavigator: true,
            data: data_array1,
            type: 'spline',
            dashStyle: 'shortdot',
            yAxis: 1,
            threshold: null,
            tooltip: {
                valueDecimals: 2,
                valueSuffix: ' ºC'
            },
            color: Highcharts.getOptions().colors[color],
            fillColor: {
                linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                },
                stops: [
                    [0, Highcharts.getOptions().colors[color]],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[color]).setOpacity(0).get('rgba')]
                ]
            }
        }, false); // add new series, don't redraw
        series1.hide();
        series2.hide();
    }
    //console.log("COLORS: "+color+": "+ chart1.series.length + ": "+ Highcharts.getOptions().colors.length );

    if (redraw == true) {
        chart1.redraw();
        chart2.redraw();
    }
}

function requestData() {
    setTimeout(requestData, timeout);
    if (live) {
        var datefrom = "" + prev.getFullYear() + "-" + (get_two_digits(prev.getMonth() + 1)) + "-" + get_two_digits(prev.getDate());
        var timefrom = "" + get_two_digits(prev.getHours()) + ":" + prev.getMinutes() + ":" + get_two_digits(prev.getSeconds());


        var data = {
            datefrom: datefrom,
            timefrom: timefrom
        };
        $.ajax({
            type: 'POST',
            url: 'querydata2',
            data: data,
            success: function (results) {
                //console.log(results)
                if (results != "NONE") {


                    var series = chart1.series[1],
                        shift = series.data.length > 1000; // shift if the series is longer than 20
                    console.log("SERIES LENGTH:" + series.data.length + " " + shift);

                    points = JSON.parse(results);
                    for (i in points) {
                        point = points[i][2];
                        var name1 = "Board-" + points[i][0] + "/Sensor-" + points[i][1];
                        var it = findIndexByName(name1);
                        if (it[0] == -1 && it[1] == -1) {
                            console.log("Not found series");
                            add_new_series(name1, point, true, points[i][3])
                        }//End if
                        else {
                            console.log("Adding Point");
                            for (var j = 0; j < point.length; j++) {
                                var date_point = new Date(point[j].date)
                                if (it[0] != -1) {
                                    chart1.series[it[0]].addPoint([date_point.getTime(), point[j].cal_data], true, shift);
                                }
                                if (it[1] != -1) {
                                    chart2.series[it[1]].addPoint([date_point.getTime(), point[j].cal_data], true, shift);
                                }

                            }
                            var prev_dat = new Date(point[point.length - 1].date);

                            if (prev < prev_dat) {

                                prev = prev_dat;
                            }
                        }//End else

                    }
                }
                console.log("PREV:" + prev);

            },
            cache: false
        });
    }
}

$(document).ready(function () {
    $(".datepicker-modal").css("color", "#1a237e");
    $(".datepicker-date-display").css("background-color", "#1a237e");
    $(".dropdown-content>li>a").css("color", "#1a237e");
    $('.tooltipped').tooltip();


});

document.addEventListener('DOMContentLoaded', function () {

    $('.sidenav').sidenav();
    $("#no_content_modal").modal({
        dismissible: false
    });

    $('#datefrom').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#timefrom').timepicker({
        twelveHour: false,
        defaultTime: '00:00'
    });
    $('#dateto').datepicker({
        format: 'yyyy-mm-dd'
    })
    $('#timeto').timepicker({
        twelveHour: false,
        defaultTime: '23:59'
    });
    check_state();

    $("#live-button").on("click", function () {
        state_live_button = 1 - state_live_button;
        set_state2(state_live_button);

    });
    $("#live-button").css('background-color', '#e53935'); //RED
    $("#live-button").css('color', '#FFFFFF'); //BLACK
    $("#live-button").css('border-color', '#FFFFFF'); //BLACK
    $("#live-button").children("i").text("fiber_manual_record");
    $("#live-button").children("span").text("Live");

    $.ajax({
        type: 'POST',
        url: 'querydata2',
        success: function (results) {
            //console.log(results);
            if (results == "Empty") {
                $("#loading").hide();
                M.Modal.getInstance($("#no_content_modal")).open();
            } else if (results != "NONE") {
                //console.log(results);
                points = JSON.parse(results);

                chart1 = gen_chart([]);
                chart2 = gen_chart2([]);
                console.log("Length of series: " + chart1.series.length);

                for (i in points) {
                    var point = points[i][2];
                    var name1 = "Board-" + points[i][0] + "/Sensor-" + points[i][1];
                    add_new_series(name1, point, false, points[i][3]);
                }
                if (chart1.series.length > 0) {
                    chart1.redraw();
                    chart1.series[0].show();
                }
                if (chart2.series.length > 0) {
                    chart2.redraw();
                    chart2.series[0].show();
                }


                //setTimeout(hide_series, 1);
                load_tags();
                requestData();
                $("#loading").hide();
            } //END IF
            else {
                M.toast({
                    html: 'There is no data currently for those sensors.'
                });
                3
            }

        },
        cache: false
    });

    $("#change_state").on("click", function () {

        if (state == 0) {
            $.ajax({
                url: 'start_measurements',
                success: function (results) {
                    console.log(results);
                    if (results == "SUCCESS") {
                        state = 1 - state;
                        set_state(state);
                        console.log("STARTED")
                    }
                },
                cache: false
            });
        } else {
            $.ajax({
                url: 'stop_measurements',
                success: function (results) {
                    console.log(results);
                    if (results == "SUCCESS") {
                        state = 1 - state;
                        set_state(state);
                        console.log("STOPED");
                    }
                },
                cache: false
            });
        }

    });

    $("#send_form").on("click", function () {
        $("#loading").show();
        console.log($("#form_send_data").serialize());
        $.ajax({
            type: 'POST',
            url: 'querydata2',
            data: $("#form_send_data").serialize(),
            success: function (results) {
                //console.log(results);
                console.log("I PASS THROUHG HERE")
                chart1 = gen_chart([]);
                chart2 = gen_chart2([]);
                if (results == "Empty") {
                    $("#loading").hide();
                    M.Modal.getInstance($("#no_content_modal")).open();
                } else if (results != "NONE") {
                    points = JSON.parse(results);
                    for (var i = 0; i < points.length; i++) {
                        var point = points[i][2];
                        var name1 = "Board-" + points[i][0] + "/Sensor-" + points[i][1];
                        add_new_series(name1, point, false, points[i][3]);
                    }
                }
                else {
                    M.toast({
                        html: 'There is no data currently for those sensors.'
                    });
                }
                if (chart1.series.length > 0) {
                    chart1.redraw();
                    chart1.series[0].show();
                }
                if (chart2.series.length > 0) {
                    chart2.redraw();
                    chart2.series[0].show();
                }
                console.log("CHANGED DATA ARRAY");
                $("#loading").hide();
            },
            cache: false
        });
    });


});

function hide_series() {

    for (var i = 1; i < chart1.series.length; i++) {
        chart1.series[i].hide();
        chart2.series[i].hide();
    }
}

function check_state() {
    $.ajax({
        url: 'is_running',
        success: function (results) {
            if (results == "true") {
                state = 1;
                set_state(state);

            } else {
                state = 0;
                set_state(state);

            }
        },
        cache: false
    });
}

function set_state(state) {
    if (state == 1) {
        $("#change_state").css('background-color', '#f44336'); //RED
        $("#change_state").children("i").text("stop");
        $("#change_state").children("span").text("Stop Measurements");
    } else {
        $("#change_state").css('background-color', '#26a69a'); //GREEN
        $("#change_state").children("i").text("play_arrow");
        $("#change_state").children("span").text("Start Measurements");

    }
}

function set_state2(state) {
    if (state == 1) {
        $("#live-button").css('background-color', '#e53935'); //RED
        $("#live-button").css('color', '#FFFFFF'); //BLACK
        $("#live-button").css('border-color', '#FFFFFF'); //BLACK
        $("#live-button").children("i").text("fiber_manual_record");
        $("#live-button").children("span").text("Live");
        $("#date_interval_form").slideToggle();
        live = true;
    } else {
        $("#live-button").css('background-color', '#43a047'); //GREEN
        $("#live-button").css('color', '#ffffff'); //BLACK
        $("#live-button").css('border-color', '#ffffff'); //BLACK
        $("#live-button").children("i").text("refresh");
        $("#live-button").children("span").text("Replay");
        $("#date_interval_form").slideToggle();
        live = false;

    }
}


function gen_chart(data_array) {
    var color = 0;
    return Highcharts.stockChart('graph1', {
        chart: {
            zoomType: 'xy',
            pinchType: 'xy'
        },
        legend: {
            enabled: true,
            align: 'left',
            layout: "horizontal",
            maxHeight: 40
        },
        rangeSelector: {
            enabled: false,
            selected: 1
        },
        title: {
            text: 'Dose'
        },
        yAxis: [{ // Primary yAxis
            title: {
                text: 'Dose [Gy]',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: false


        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: 'Temperature [ºC]',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }]

    }); //END CHART
}

function gen_chart2(data_array) {
    return Highcharts.stockChart('graph2', {
        chart: {
            zoomType: 'xy',
            pinchType: 'xy'
        },
        legend: {
            enabled: true,
            align: 'left',
            layout: "horizontal",
            maxHeight: 40
        },
        rangeSelector: {
            enabled: false,
            selected: 1
        },
        title: {
            text: 'Fluence'
        },
        yAxis: [{ // Primary yAxis
            title: {
                text: 'Fluence [n_1MeV / cm_2]',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: false
        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: 'Temperature [ºC]',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true/*,
            offset: 50*/

        }]

    }); //END CHART
}

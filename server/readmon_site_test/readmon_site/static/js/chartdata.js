var chart; // global
var chart1;
var chart2;
var chart3;
var prev;
var state;
var state_live_button = 1;
var live = true;
var timeout = 30000;


function get_two_digits(num) {
    if (num < 10) {
        return "0" + num;
    } else return num;
}

function findIndexByName(name) {
    //I know this might be terribly inefficient.
    for (var i in chart1.series) {
        //console.log(name + "==" + chart1.series[i].name);
        if (chart1.series[i].name == name) {
            return i;
        }
    }
    return -1;
}

function init_select_dropdowns() {
    var elems = document.querySelectorAll('select');
    var options = document.querySelectorAll('option');
    var instances = M.FormSelect.init(elems, options);
}

function load_tags() {
    $.ajax({
        type: 'GET',
        url: 'tags',
        success: function (results) {
            //console.log(results)
            results = JSON.parse(results);
            if (results != "NONE") {
                var name = "#tag"
                var txt = "";
                for (var i in results) {
                    txt += "<option value=\"" + results[i] + "\">" + results[i] + "</option>";
                }
                $(name).append(txt);
                init_select_dropdowns();
            }

            console.log("PREV:" + prev);

        },
        cache: false
    });
}

function add_new_series(name, point, redraw) {

    var data_array1 = new Array();
    var data_array2 = new Array();
    var data_array3 = new Array();
    for (var it = 0; it < point.length; it++) {
        date_entry = new Date(point[it].date);
        var tuple = [date_entry.getTime(), point[it].vmeas];
        var tuple2 = [date_entry.getTime(), point[it].imeas];
        var tuple3 = [date_entry.getTime(), point[it].temp];
        data_array1.push(tuple);
        data_array2.push(tuple2);
        data_array3.push(tuple3);
    }
    var prev_dat = new Date(point[point.length - 1].date);
    if (prev == undefined || prev < prev_dat) {
        prev = prev_dat;
    }

    var name1 = name;
    var color = (chart1.series.length / 2) % (Highcharts.getOptions().colors.length);

    //console.log("COLORS: "+color+": "+ chart1.series.length + ": "+ Highcharts.getOptions().colors.length );
    var series1 = chart1.addSeries({
        name: name1,
        showInNavigator: true,
        data: data_array1,
        type: 'spline',
        yAxis: 0,
        threshold: null,
        tooltip: {
            valueDecimals: 2,
            valueSuffix: ' V'
        },
        color: Highcharts.getOptions().colors[color],
        fillColor: {
            linearGradient: {
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1
            },
            stops: [
                [0, Highcharts.getOptions().colors[color]],
                [1, Highcharts.Color(Highcharts.getOptions().colors[color]).setOpacity(0).get('rgba')]
            ]
        }
    }, false); // add new series, don't redraw
    var series2 = chart2.addSeries({
        name: name1,
        showInNavigator: true,
        data: data_array2,
        type: 'spline',
        yAxis: 0,
        threshold: null,
        tooltip: {
            valueDecimals: 2,
            valueSuffix: ' mA'
        },
        color: Highcharts.getOptions().colors[color],
        fillColor: {
            linearGradient: {
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1
            },
            stops: [
                [0, Highcharts.getOptions().colors[color]],
                [1, Highcharts.Color(Highcharts.getOptions().colors[color]).setOpacity(0).get('rgba')]
            ]
        }
    }, false); // add new series, don't redraw
    var series3 = chart3.addSeries({
        name: name1,
        showInNavigator: true,
        data: data_array3,
        type: 'spline',
        yAxis: 0,
        threshold: null,
        tooltip: {
            valueDecimals: 2,
            valueSuffix: ' ºC'
        },
        color: Highcharts.getOptions().colors[color],
        fillColor: {
            linearGradient: {
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1
            },
            stops: [
                [0, Highcharts.getOptions().colors[color]],
                [1, Highcharts.Color(Highcharts.getOptions().colors[color]).setOpacity(0).get('rgba')]
            ]
        }
    }, false); // add new series, don't redraw
    series1.hide();
    series2.hide();
    series3.hide();
    if (redraw == true) {
        chart1.redraw();
        chart2.redraw();
        chart3.redraw();
    }
}

function requestData() {
    setTimeout(requestData, timeout);
    if (live) {
        var datefrom = "" + prev.getFullYear() + "-" + (get_two_digits(prev.getMonth() + 1)) + "-" + get_two_digits(prev.getDate());
        var timefrom = "" + get_two_digits(prev.getHours()) + ":" + prev.getMinutes() + ":" + get_two_digits(prev.getSeconds());


        var data = {
            datefrom: datefrom,
            timefrom: timefrom
        };
        $.ajax({
            type: 'POST',
            url: 'querydata',
            data: data,
            success: function (results) {
                //console.log(results)
                if (results != "NONE") {


                    var series = chart1.series[1],
                        shift = series.data.length > 1000; // shift if the series is longer than 20
                    console.log("SERIES LENGTH:" + series.data.length + " " + shift);

                    points = JSON.parse(results);
                    for (i in points) {
                        point = points[i][2];
                        var name1 = "Board-" + points[i][0] + "/Sensor-" + points[i][1];
                        var it = findIndexByName(name1);
                        if (it == -1) {
                            console.log("Adding new series");
                            add_new_series(name1, point, true);
                        }
                        else {
                            for (var j = 0; j < point.length; j++) {
                                console.log("Adding new points");
                                var date_point = new Date(point[j].date)
                                chart1.series[it].addPoint([date_point.getTime(), point[j].vmeas], true, shift);
                                chart2.series[it].addPoint([date_point.getTime(), point[j].imeas], true, shift);
                                chart3.series[it].addPoint([date_point.getTime(), point[j].temp], true, shift);

                            }
                            var prev_dat = new Date(point[point.length - 1].date);

                            if (prev < prev_dat) {

                                prev = prev_dat;
                            }
                        }

                    }
                }
                console.log("PREV:" + prev);

            },
            cache: false
        });
    }
}

$(document).ready(function () {
    $(".datepicker-modal").css("color", "#1a237e");
    $(".datepicker-date-display").css("background-color", "#1a237e");
    $(".dropdown-content>li>a").css("color", "#1a237e");
    $('.tooltipped').tooltip();


});

document.addEventListener('DOMContentLoaded', function () {
$('.sidenav').sidenav();
    $("#no_content_modal").modal({
        dismissible: false
    });

    $('#datefrom').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#timefrom').timepicker({
        twelveHour: false,
        defaultTime: '00:00'
    });
    $('#dateto').datepicker({
        format: 'yyyy-mm-dd'
    })
    $('#timeto').timepicker({
        twelveHour: false,
        defaultTime: '23:59'
    });
    check_state();

    $("#live-button").on("click", function () {
        state_live_button = 1 - state_live_button;
        set_state2(state_live_button);

    });
    $("#live-button").css('background-color', '#e53935'); //RED
    $("#live-button").css('color', '#FFFFFF'); //BLACK
    $("#live-button").css('border-color', '#FFFFFF'); //BLACK
    $("#live-button").children("i").text("fiber_manual_record");
    $("#live-button").children("span").text("Live");

    $.ajax({
        type: 'POST',
        url: 'querydata',
        success: function (results) {
            //console.log(results);
            if (results == "Empty") {
                $("#loading").hide();
                M.Modal.getInstance($("#no_content_modal")).open();
            } else if (results != "NONE") {

                points = JSON.parse(results);

                chart1 = gen_chart([]);
                chart2 = gen_chart2([]);
                chart3 = gen_chart3([]);
                console.log(chart1.series.length);
                console.log(chart2.series.length);
                console.log(chart3.series.length);
                for (i in points) {
                    var data_array1 = new Array();
                    var data_array2 = new Array();
                    var data_array3 = new Array();
                    var point = points[i][2];
                    var name1 = "Board-" + points[i][0] + "/Sensor-" + points[i][1];
                    add_new_series(name1, point, false);
                }
                chart1.series[0].show();
                chart2.series[0].show();
                chart3.series[0].show();
                chart1.redraw();
                chart2.redraw();
                chart3.redraw();
                //setTimeout(hide_series, 1);

                load_tags();
                requestData();
                $("#loading").hide();
            } //END IF
            else {
                M.toast({
                    html: 'There is no data currently for those sensors.'
                });
            }

        },
        cache: false
    });

    $("#change_state").on("click", function () {

        if (state == 0) {
            $.ajax({
                url: 'start_measurements',
                success: function (results) {
                    console.log(results);
                    if (results == "SUCCESS") {
                        state = 1 - state;
                        set_state(state);
                        console.log("STARTED")
                    }
                },
                cache: false
            });
        } else {
            $.ajax({
                url: 'stop_measurements',
                success: function (results) {
                    console.log(results);
                    if (results == "SUCCESS") {
                        state = 1 - state;
                        set_state(state);
                        console.log("STOPED");
                    }
                },
                cache: false
            });
        }

    });

    $("#send_form").on("click", function () {
        $("#loading").show();
        console.log($("#form_send_data").serialize());
        $.ajax({
            type: 'POST',
            url: 'querydata',
            data: $("#form_send_data").serialize(),
            success: function (results) {
                console.log(results);
                console.log("I PASS THROUHG HERE")
                chart1 = gen_chart([]);
                chart2 = gen_chart2([]);
                chart3 = gen_chart3([]);
                if (results == "Empty") {
                    $("#loading").hide();
                    M.Modal.getInstance($("#no_content_modal")).open();
                } else if (results != "NONE") {
                    points = JSON.parse(results);
                    for (var i in points) {
                        var point = points[i][2];
                        var name1 = "Board-" + points[i][0] + "/Sensor-" + points[i][1];
                        add_new_series(name1, point, false);
                    }
                    chart1.redraw();
                    chart1.series[0].show();
                    chart2.redraw();
                    chart2.series[0].show();
                    chart3.redraw();
                    chart3.series[0].show();

                }
                else {
                    M.toast({
                        html: 'There is no data currently for those sensors.'
                    });
                }


                console.log("CHANGED DATA ARRAY");
                $("#loading").hide();
            },
            cache: false
        });
    });


});

function hide_series() {

    for (var i = 1; i < chart1.series.length; i++) {
        chart1.series[i].hide();
        chart2.series[i].hide();
        chart3.series[i].hide();
    }
}

function check_state() {
    $.ajax({
        url: 'is_running',
        success: function (results) {
            if (results == "true") {
                state = 1;
                set_state(state);

            } else {
                state = 0;
                set_state(state);

            }
        },
        cache: false
    });
}

function set_state(state) {
    if (state == 1) {
        $("#change_state").css('background-color', '#f44336'); //RED
        $("#change_state").children("i").text("stop");
        $("#change_state").children("span").text("Stop Measurements");
    } else {
        $("#change_state").css('background-color', '#26a69a'); //GREEN
        $("#change_state").children("i").text("play_arrow");
        $("#change_state").children("span").text("Start Measurements");

    }
}

function set_state2(state) {
    if (state == 1) {
        $("#live-button").css('background-color', '#e53935'); //RED
        $("#live-button").css('color', '#FFFFFF'); //BLACK
        $("#live-button").css('border-color', '#FFFFFF'); //BLACK
        $("#live-button").children("i").text("fiber_manual_record");
        $("#live-button").children("span").text("Live");
        $("#date_interval_form").slideToggle();
        live = true;
        //requestData();
    } else {
        $("#live-button").css('background-color', '#43a047'); //GREEN
        $("#live-button").css('color', '#ffffff'); //BLACK
        $("#live-button").css('border-color', '#ffffff'); //BLACK
        $("#live-button").children("i").text("refresh");
        $("#live-button").children("span").text("Replay");
        $("#date_interval_form").slideToggle();
        live = false;

    }
}

function gen_chart(data_array) {
    var color = 0;
    return Highcharts.stockChart('graph1', {
        chart: {
            zoomType: 'xy',
            pinchType: 'xy'
        },
        legend: {
            enabled: true,
            align: 'left',
            layout: "horizontal",
            maxHeight: 40
        },
        /*legend: {
            enabled: true,
            align: 'right',
            layout: 'vertical',
            verticalAlign: 'top',
            maxHeight: 500,
            y: 30
        },*/
        rangeSelector: {
            enabled: false,
            selected: 1
        },
        title: {
            text: 'Voltage vs Time'
        },
        yAxis: [{ // Primary yAxis
            title: {
                text: 'Voltage [V]',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: false
        }]
        /*,

                            colors: [Highcharts.getOptions().colors[color] ,'red', 'orange', 'green', 'blue', 'purple', 'brown'],

                            series: [{
                                name: 'Voltage at Time',
                                data: data_array,
                                type: 'areaspline',
                                threshold: null,
                                tooltip: {
                                    valueDecimals: 2
                                },
                                fillColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, Highcharts.getOptions().colors[color]],
                                        [1, Highcharts.Color(Highcharts.getOptions().colors[color]).setOpacity(0).get('rgba')]
                                    ]
                                }
                            }]*/
    }); //END CHART
}

function gen_chart2(data_array) {
    var color = 3;
    return Highcharts.stockChart('graph2', {
        chart: {
            zoomType: 'xy',
            pinchType: 'xy'
        },
        legend: {
            enabled: true,
            align: 'left',
            layout: "horizontal",
            maxHeight: 40
        },

        rangeSelector: {
            enabled: false,
            selected: 1
        },
        title: {
            text: 'Current vs Time'
        },
        yAxis: [{ // Primary yAxis
            title: {
                text: 'Intensity [mA]',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: false
        }]
        /*,
                            colors: [Highcharts.getOptions().colors[color] ,'red', 'orange', 'green', 'blue', 'purple', 'brown'],

                            series: [{
                                name: 'Current at Time',
                                data: data_array,
                                type: 'areaspline',
                                threshold: null,
                                tooltip: {
                                    valueDecimals: 2
                                },
                                fillColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, Highcharts.getOptions().colors[color]],
                                        [1, Highcharts.Color(Highcharts.getOptions().colors[color]).setOpacity(0).get('rgba')]
                                    ]
                                }
                            }]*/
    }); //END CHART
}

function gen_chart3(data_array) {
    var color = 2;
    return Highcharts.stockChart('graph3', {
        chart: {
            zoomType: 'xy',
            pinchType: 'xy'
        },
        legend: {
            enabled: true,
            align: 'left',
            layout: "horizontal",
            maxHeight: 40
        },
        rangeSelector: {
            enabled: false,
            selected: 1
        },
        title: {
            text: 'Temperature vs Time'
        },
        yAxis: [{ // Primary yAxis
            title: {
                text: 'Temperature [ºC]',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: false
        }]
        /*,

                            colors: [Highcharts.getOptions().colors[color] ,'red', 'orange', 'green', 'blue', 'purple', 'brown'],

                            series: [{
                                name: 'Temperature at Time',
                                data: data_array,
                                type: 'areaspline',
                                threshold: null,
                                tooltip: {
                                    valueDecimals: 2
                                },
                                fillColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, Highcharts.getOptions().colors[color]],
                                        [1, Highcharts.Color(Highcharts.getOptions().colors[color]).setOpacity(0).get('rgba')]
                                    ]
                                }
                            }]*/
    }); //END CHART
}

/*$(function () {
    var myChart = Highcharts.chart('graph2', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Fruit Consumption'
        },
        xAxis: {
            categories: ['Apples', 'Bananas', 'Oranges']
        },
        yAxis: {
            title: {
                text: 'Fruit eaten'
            }
        },
        series: [{
            name: 'Jane',
            data: [1, 0, 4]
        }, {
            name: 'John',
            data: [5, 7, 3]
        }]
    });
});*/
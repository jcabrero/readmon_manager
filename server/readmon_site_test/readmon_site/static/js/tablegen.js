var number_of_sensors = 0;
var sensors = null;
var table_row;
var max_sensors = 8;
var channels = 11;
var calibration_number = 1;
var options_text;

function init_select_dropdowns() {
    var elems = document.querySelectorAll('select');
    var options = document.querySelectorAll('option');
    var instances = M.FormSelect.init(elems, options);
}

function bind_intervals() {
    $(".interval_starter").bind("input", function () {
        var num = parseInt(this.name.substr(3, this.name.length)) - 1;
        var name = "#ine" + num;
        $(name).val($(this).val());
    });

    $(".interval_ender").bind("input", function () {
        var num = parseInt(this.name.substr(3, this.name.length)) + 1;
        var name = "#ini" + num;
        $(name).val($(this).val());
    });
}

document.addEventListener('DOMContentLoaded', function () {
    console.log("second")

    $("#add_button").on("click", function () {

        console.log("CLICK!");
        //init_select_dropdowns();
        generate_row(sensors);


    });


    $("#store_now").on("click", function () {
        send_changes_to_server();
    });

    $("#store_settings").on("click", function () {
        download("settings.txt", get_array_current_values());
    });

    $("#save_sensor").on("click", function () {
        update_sensor();
    });

    $("#remove-def").on("click", function () {

        delete_sensor();
    });

    $("#new_sensor_save").on("click", function () {
        create_sensor();
    });

    $("#add_new_calibration").on("click", function () {
        add_new_calibration_row();
    });

    bind_intervals();

    $("#server_store_calibration").on("click", function () {
        server_store_calibration();
    });

    $("#remove_calibration").on("click", function () {
        remove_row();
    });
});


$(document).ready(function () {
    $('.sidenav').sidenav();
    $('.modal').modal();
    $('.tooltipped').tooltip();
    console.log("first");
    get_remote();

});

//Generates a row of content based on the results passed as parameter
function generate_row(results) {
    if (number_of_sensors == max_sensors - 1) {
        $("#add_button").remove();
        console.log("removed");
    }
    if (number_of_sensors == max_sensors) return null;
    var name = "#table-body";
    var index = number_of_sensors++;
    var color = ((index % 2) == 0) ? "grey lighten-3" : "grey lighten-2";

    var txt = "<tr>\n\
	\t<td>\n\
	\t\t<input id=\"sensor_name_" + index + "\" name=\"sensor_name_" + index + "\" value=\"Sensor " + (index) + "\" type=\"text\">\n\
	\t</td>\n\
	\t<td>\n\
	\t\t<select name=\"sensor_connector_" + index + "\" id=\"sensor_connector_" + index + "\">\n";
    for (var j = 0; j < max_sensors; j++) {
        txt += "\t\t\t<option value=\"" + j + "\">" + j + "</option>\n";
    }
    txt += "\t\t</select>\n\
	\t</td>\n";

    for (var i = 0; i < channels; ++i) {
        txt += "\t<td>\n\
		\t\t<select name=\"sensor" + index + "-" + i + "\" id=\"sensor" + index + "-" + i + "\">\n";
        txt += "\t\t\t<option value=\"none\"> None </option>\n"
        for (var j = 0; j < results.length; j++) {
            //console.log(results[j]);
            txt += "\t\t\t<option value=\"" + results[j].sensor_name + "\"> " + results[j].sensor_name + " </option>\n"
        }
        txt += "\t\t</select>\n\
    	\t</td>\n"
    }
    txt += "</tr>\n";
    //console.log(txt);
    console.log("Created a new row");

    $(name).append(txt);
    init_select_dropdowns()
    return txt;
}


//Gets the data of the sensors from the database, and creates the HTML equivalent code dynamically.
function get_remote() {
    var i = $.ajax({
        type: "GET",
        url: 'sensors',
        dataType: 'json',
        success: function (results) {
            console.log("LOADING SENSORS...");
            //console.log(results);
            //$('#table-body tr:last').after(result);
            //$('#myTable > div:last-child').append(result);
            sensors = results;
            console.log("LOADED SENSORS...");
            load_settings();
        }
    });
    return i;
}

function load_settings() {
    $.ajax({
        type: "POST",
        url: 'stored_settings',
        dataType: 'json',
        error: function (xhr, error) {
            console.log("XHR: ");
            console.log(xhr);
            console.log("ERROR: " + error);
        },
        success: function (results) {
            console.log("LOADING SETTINGS...");
            console.log(results);
            if (results != "NONE") {
                object = results;
                console.log(object);
                var last_row = -1;
                var row_index = -1;
                for (var i in object) {
                    console.log(object[i]);
                    if (last_row != object[i][1]) {
                        row_index++;
                        generate_row(sensors);
                        last_row = object[i][1];
                        var sensor_name = "#sensor_name_" + row_index;
                        $(sensor_name).val(object[i][0]);
                        var sensor_connector = "#sensor_connector_" + row_index;
                        $(sensor_connector).val(object[i][1]);
                        //console.log(sensor_name + ":" + sensor_connector);

                    }

                    var name = "#sensor" + row_index + "-" + object[i][3];
                    console.log(name);
                    $(name).val(object[i][2]);

                }
            }
            else {
                generate_row(sensors);
            }
            generate_modal_dropdown();
            load_sensor_data_modal(0);


        }
    });
}

function enable_html() {
    $('.dropdown-trigger').dropdown();
    init_select_dropdowns();


    $("#modal-dropdown").change(function () {
        load_sensor_data_modal(get_sensor_index(get_value_from_select_form("modal-dropdown", "value")));
    });
}

function download(filename, text) {
    if (text == "") {
        return;
    }
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function validate_values() {
    var arr = new Array();
    for (var i = 0; i < max_sensors; i++) {
        arr.push(false);
    }
    for (var j = 0; j < max_sensors; j++) {
        var key_out = "sensor_connector_" + j;
        elem = document.getElementById(key_out);
        if (elem != null) {

            var elem_index = elem.options[elem.selectedIndex].value;
            if (arr[elem_index] == true) {
                return false;
            }
            arr[elem_index] = true;
        }
    }
    return true;
}

function get_value_from_input_form(name) {
    var elem = document.getElementById(name);
    if ((elem != undefined) && (elem != null)) {
        return elem.value;
    }
    else {
        return "None";
    }
}

function get_value_from_select_form(name, what = "value") {
    var elem = document.getElementById(name);
    if (elem != null) {
        console.log("ELEM: " + elem);
        if (what == "value") {
            return elem.options[elem.selectedIndex].value;
        }
        return elem.options[elem.selectedIndex].text;
    }
    else {
        return "None"
    }
}

function get_array_current_values() {
    var ret = "";
    console.log("Im here")
    if (!validate_values()) {
        M.toast({html: 'Wrong settings: Sensors are repeated.'})
        return "";
    }
    for (var j = 0; j < max_sensors; j++) {
        var sensor_name = "sensor_name_" + j;
        ret += get_value_from_input_form(sensor_name) + "\t";
        var sensor_connector = "sensor_connector_" + (j);
        ret += get_value_from_select_form(sensor_connector, "value") + "\t";
        for (var i = 0; i < channels; ++i) {
            var key = "sensor" + j + "-" + i;
            //console.log(key);
            ret += get_value_from_select_form(key, "text")
            if (i != (channels - 1)) {
                ret += "\t"
            }
        }
        ret += "\n"
    }

    console.log(ret);
    return ret;
}

function generate_modal_dropdown() {
    console.log("Generating modal dropdown");
    var txt = "";
    var name = "#modal-dropdown";
    for (var i in sensors) {
        txt += "<option value=\"" + sensors[i].sensor_name + "\"> " + sensors[i].sensor_name + " </option>\n"
    }
    $(name).append(txt);
    generate_possible_actions();

}

function generate_possible_actions() {
    $.ajax({
        type: "GET",
        url: 'functions',
        dataType: "json",
        error: function (xhr, error) {
            console.log("XHR: ");
            console.log(xhr);
            console.log("ERROR: " + error);
        },
        success: function (results) {
            console.log(results);
            generate_dropdown_actions(results);
        }
    });

}

function generate_dropdown_actions(rows) {
    var name = ".class_of";
    var txt = "";
    for (i in rows) {
        txt += "<option value=\"" + rows[i] + "\">" + rows[i] + "</option>";
    }
    options_text = txt;
    $(name).append(txt);
    enable_html();
}

function get_sensor_index(name) {
    for (var i = 0; i < sensors.length; i++) {
        if (sensors[i].sensor_name == name) {
            return i;
        }
    }
    return -1;
}

function load_sensor_data_modal(sensor_id) {
    var sensor = sensors[sensor_id];
    $("#sensor_name").val(sensor.sensor_name);
    $("#vset").val(sensor.vset);
    $("#iset").val(sensor.iset);
    $("#ramp_time").val(sensor.ramp_time);
    $("#settle_time").val(sensor.settle_time);
    $("#ramping_steps").val(sensor.ramping_steps);
    $("#max_measurement_time").val(sensor.max_measurement_time);
    //$("#class_of").val(sensor.measurement_type);
    console.log(sensor.measurement_type);
    if (sensor.measurement_type == "Fluence") {
        $("input[name=group1]:first").prop('checked', true);
    }
    else if (sensor.measurement_type == "Dose") {

        $("input[name=group1]:eq(1)").prop('checked', true);
    }
    else {
        $("input[name=group1]:last").prop('checked', true)
    }

    if (is_original(sensor.sensor_name)) {
        $("#remove_sensor").hide();
    }
    else {
        $("#remove_sensor").show();
    }
    console.log(sensor);
    get_calibrations_for_sensor(sensor.sensor_name);

}

function create_sensor() {
    var new_sensor = {
        sensor_name: $("#new_sensor_name").val(),
        vset: $("#new_vset").val(),
        iset: $("#new_iset").val(),
        ramp_time: $("#new_ramp_time").val(),
        settle_time: $("#new_settle_time").val(),
        ramping_steps: $("#new_ramping_steps").val(),
        max_measurement_time: $("#new_max_measurement_time").val(),
        measurement_type: $("input[name=new_class_of]:checked").next().text()
    }
    $.ajax({
        type: "POST",
        url: 'sensors',
        data: new_sensor,
        error: function (xhr, error) {
            console.log("XHR: ");
            console.log(xhr);
            console.log("ERROR: " + error);
        },
        success: function (results) {
            console.log("SENSORS INSERTED - " + results);
            if (results == "OK") {
                console.log("Sensor Inserted Correctly");
                M.toast({html: 'New sensor created'});
                console.log("RELOADING");
                location.reload();


            }
            else {
                M.toast({html: results});
            }

        }
    });

    console.log(new_sensor);
}

function update_sensor() {
    var index = get_sensor_index(get_value_from_select_form("modal-dropdown", "value"));
    var old_name = sensors[index].sensor_name;
    sensors[index].sensor_name = $("#sensor_name").val();
    sensors[index].vset = $("#vset").val();
    sensors[index].iset = $("#iset").val();
    sensors[index].ramp_time = $("#ramp_time").val();
    sensors[index].settle_time = $("#settle_time").val();
    sensors[index].ramping_steps = $("#ramping_steps").val();
    sensors[index].max_measurement_time = $("#max_measurement_time").val();
    sensors[index].measurement_type = $("input[name=group1]:checked").next().text();

    var dropdown_entry = $("#modal-dropdown").find("[value='" + old_name + "']");
    dropdown_entry.val(sensors[index].sensor_name);
    dropdown_entry.text(sensors[index].sensor_name);
    var sens_data = sensors[index];
    sens_data.old_name = old_name;
    console.log(sens_data);
    $.ajax({
        type: "PUT",
        url: 'sensors',
        data: sens_data,
        error: function (xhr, error) {
            console.log("XHR: ");
            console.log(xhr);
            console.log("ERROR: " + error);
        },
        success: function (results) {
            console.log("SENSORS UPDATED - " + results);
            if (results == "OK") {
                console.log("Record Updated Correctly");
                M.toast({html: 'Sensor correctly updated'});
                $('select').formSelect(); //Reloads form, otherwise changes are not visible.

            }

        }
    });
}


function delete_sensor() {
    var index = get_sensor_index(get_value_from_select_form("modal-dropdown", "value"));
    var old_name = sensors[index].sensor_name;
    var sens_data = {
        sensor_name: old_name
    };

    $.ajax({
        type: "DELETE",
        url: 'sensors',
        data: sens_data,
        error: function (xhr, error) {
            console.log("XHR: ");
            console.log(xhr);
            console.log("ERROR: " + error);
        },
        success: function (results) {
            console.log("SENSORS UPDATED - " + results);
            if (results == "OK") {
                console.log("Record Removed Correctly");
                M.toast({html: 'Sensor removed'});
                elem = document.getElementById("modal-dropdown");
                elem.options[elem.selectedIndex].remove();
                $('select').formSelect(); //Reloads form, otherwise changes are not visible.
                load_sensor_data_modal(0);


            }

        }
    });
}

function is_original(name) {
    if (name == "LBSD Si - 1" || name == "LBSD Si - 2" || name == "BPW" || name == "REM" || name == "LAAS" || name == "NTC") {
        return true;
    }
    return false;
}

function load_data_for_sensor(index, row_data) {
    console.log(row_data[0]);
    $("#class_of" + index).val(row_data[0]);
    $("#a" + index).val(row_data[1]);
    $("#b" + index).val(row_data[2]);
    $("#ini" + index).val(row_data[3]);
    $("#ine" + index).val(row_data[4]);
}

function get_calibrations_for_sensor(sensor_name) {
    console.log("get_calibration_for_sensor(" + sensor_name + ")");
    var data = {
        sensor_name: sensor_name
    };
    $.ajax({
        type: "GET",
        url: 'calibration',
        dataType: 'json',
        data: data,
        error: function (xhr, error) {
            console.log("XHR: ");
            console.log(xhr);
            console.log("ERROR: " + error);
        },
        success: function (results) {

            if (results != "ERROR") {
                initialize_calibrations();

                for (var i = 1; i < results.length; i++) {
                    add_new_calibration_row();
                }
                //init_select_dropdowns();
                for (i in results) {
                    load_data_for_sensor(i, results[i]);
                }
                init_select_dropdowns();

            }


        }
    });
}


function initialize_calibrations() {

    var name = "#calibration_table_body";
    $(name).children().remove();
    calibration_number = 1;
    var txt = "               <tr>\n" +
        "                    <td>\n" +
        "                        <select name=\"class_of0\" id=\"class_of0\" class=\"class_of\">\n" + options_text +
        "                        </select>\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <input id=\"a0\" type=\"text\" value=\"1\">\n" +
        "                        <label for=\"a0\">a</label>\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <input id=\"b0\" class=\"\" type=\"text\" value=\"1\">\n" +
        "                        <label for=\"b0\">b</label>\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <input id=\"ini0\" name=\"ini0\" type=\"text\" class=\"interval_starter\" value=\"0\" disabled>\n" +
        "                        <label for=\"ini0\">Interval Start</label>\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <input id=\"ine0\" name=\"ine0\" type=\"text\" class=\"interval_ender\" value=\"125\" disabled>\n" +
        "                        <label for=\"ine0\">Interval End</label>\n" +
        "                    </td>\n" +
        "                </tr>";
    $(name).append(txt);

}

function add_new_calibration_row() {
    var name = "#calibration_table_body";
    var txt = "                <tr>\n" +
        "                    <td>\n" +
        "                        <select name=\"class_of" + calibration_number + "\" id=\"class_of" + calibration_number + "\" class=\"class_of\">\n" + options_text +
        "                        </select>\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <input id=\"a" + calibration_number + "\" type=\"text\" value=\"1\">\n" +
        "                        <label for=\"a" + calibration_number + "\">a</label>\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <input id=\"b" + calibration_number + "\" class=\"\" type=\"text\" value=\"1\">\n" +
        "                        <label for=\"b" + calibration_number + "\">b</label>\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <input id=\"ini" + calibration_number + "\" name=\"ini" + calibration_number + "\" type=\"text\" class=\"interval_starter\">\n" +
        "                        <label for=\"ini" + calibration_number + "\">Interval Start</label>\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <input id=\"ine" + calibration_number + "\" name=\"ine" + calibration_number + "\" type=\"text\" class=\"interval_ender\">\n" +
        "                        <label for=\"ine" + calibration_number + "\">Interval End</label>\n" +
        "                    </td>\n" +
        "                </tr>";

    $(".interval_ender:last").prop('disabled', false);
    $(".interval_ender:last").val(undefined);
    calibration_number++;
    $(name).append(txt);
    init_select_dropdowns();
    bind_intervals();
    $(".interval_ender:last").prop('disabled', true);
    $(".interval_ender:last").val(125);


}

function remove_row() {
    if (calibration_number == 1) {
        return;
    }
    calibration_number--;
    var name = "#calibration_table_body";
    $(name).children().last().remove();
    $(".interval_ender:last").prop('disabled', true);
    $(".interval_ender:last").val(125);

}

function server_store_calibration() {
    var sensor_name = $("#sensor_name").val();
    var arr = new Array();
    for (var i = 0; i < calibration_number; i++) {
        var val_a = $("#a" + i).val();
        var val_b = $("#b" + i).val();
        var interval_start = $("#ini" + i).val();
        var interval_end = $("#ine" + i).val();
        var function_type = get_value_from_select_form("class_of" + i);
        arr.push({
            sensor_name: sensor_name,
            function_type: function_type,
            parameter_a: val_a,
            parameter_b: val_b,
            interval_start: interval_start,
            interval_end: interval_end
        });
    }
    var stringified = JSON.stringify(arr);
    var data = {
        calibrations: stringified
    }

    console.log(arr);

    $.ajax({
        type: "POST",
        url: 'calibration',
        dataType: 'json',
        data: data,
        error: function (xhr, error) {
            console.log("XHR: ");
            console.log(xhr);
            console.log("ERROR: " + error);
        },
        success: function (results) {

            if (results == "ERROR") {
                M.toast({html: 'The calibrations supplied are incorrect'});
            }
            if (results == "OK") {
                M.toast({html: 'Calibrations updated correctly'});
            }


        }
    });
}

function send_changes_to_server() {
    if (!validate_values()) {
        M.toast({html: 'Wrong settings: Sensors are repeated.'})
        return "";
    }
    //(user_given_sensor_name,  board_num, dosimeter_name, channel)
    var arr_values = new Array();
    for (var i = 0; i < number_of_sensors; i++) {
        var sensor_name = get_value_from_input_form("sensor_name_" + i);
        for (var j = 0; j < 11; j++) {
            var nametouse = "sensor" + i + "-" + j;
            var value = get_value_from_select_form(nametouse, "text");
            if (value != "None") {
                arr_values.push([sensor_name, i, value, j]);
            }
        }
    }

    console.log(arr_values);
    var stringified = JSON.stringify(arr_values);
    var object = {
        data: stringified
    }
    $.ajax({
        type: "PUT",
        url: 'upload',
        dataType: 'json',
        data: object,
        success: function (results) {

        }
    });
}
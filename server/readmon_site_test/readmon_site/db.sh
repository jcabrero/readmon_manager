#!/bin/sh
DIRECTORY=`pwd`

if [ "$1" == "remove" ]
then
  echo "REMOVING PREVIOUS ENTRIES IN DB..."
  mysql -uroot -parduino -e"source $DIRECTORY/db_rm.sql"
  echo "DONE!"
elif [ "$1" == "all" ]
then
  echo "REMOVING PREVIOUS ENTRIES IN DB..."
  mysql -uroot -parduino -e"source $DIRECTORY/db_rm.sql"
  echo "CREATING DB..."
  mysql -uroot -parduino -e"source $DIRECTORY/db_cr.sql"
  echo "INSERTING TEST ENTRIES TO DB ..."
  mysql -uroot -parduino -e"source $DIRECTORY/db_in.sql"
  echo "DONE!"
elif [ "$1" == "create" ]
then
  echo "CREATING DB..."
  mysql -uroot -parduino -e"source $DIRECTORY/db_cr.sql"
  echo "DONE!"
elif [ "$1" == "insert" ]
then
  echo "INSERTING TEST ENTRIES TO DB ..."
  mysql -uroot -parduino -e"source $DIRECTORY/db_in.sql"
  echo "DONE!"
elif [ "$1" == "select" ]
then
  echo "EXTRACTING ENTRIES FROM DB ..."
  mysql -uroot -parduino -e"source $DIRECTORY/db_sel.sql"
  echo "DONE!"
else
  echo "Usage: ./db create|remove|all|insert"
fi
#mysql -uroot -parduino -e"source ./db_cr.sql"

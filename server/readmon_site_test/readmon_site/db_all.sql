-- DB CREATION SCRIPT
DROP DATABASE IF EXISTS `RADMON`;
CREATE DATABASE IF NOT EXISTS `RADMON`;
USE RADMON;
DROP TABLE IF EXISTS test_table;

COMMIT;
-- CHECK THE ONLY DATABASE ACCESSIBLE IS THE ONE WE GRANTED PRIVILEGES TO
-- SHOW DATABASES;
-- Set as default database the GigAvenueDB

CREATE TABLE test_table (
board_id INT NOT NULL,
radmon_board_num INT NOT NULL,
sensor_num INT NOT NULL,
tag varchar(50) NOT NULL,
PRIMARY KEY(board_id, radmon_board_num, sensor_num)
);

INSERT into test_table(board_id, radmon_board_num, sensor_num, tag) values (1, 1, 1, "tag1");
INSERT into test_table(board_id, radmon_board_num, sensor_num, tag) values (2, 1, 1, "tag1");
INSERT into test_table(board_id, radmon_board_num, sensor_num, tag) values (3, 1, 1, "tag1");
COMMIT;


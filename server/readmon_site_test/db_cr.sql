-- DB CREATION SCRIPT
CREATE DATABASE IF NOT EXISTS `RADMON`;
USE RADMON;
-- CHECK THE ONLY DATABASE ACCESSIBLE IS THE ONE WE GRANTED PRIVILEGES TO
-- SHOW DATABASES;

CREATE TABLE IF NOT EXISTS test_table (
  board_id         INT         NOT NULL,
  radmon_board_num INT         NOT NULL,
  sensor_num       INT         NOT NULL,
  tag              varchar(50) NOT NULL,
  PRIMARY KEY (board_id, radmon_board_num, sensor_num)
);

CREATE TABLE IF NOT EXISTS user (
  username varchar(50) NOT NULL,
  password varchar(50) NOT NULL,
  PRIMARY KEY (username)
);

CREATE TABLE IF NOT EXISTS sensor (
  sensor_name          varchar(50)  NOT NULL,
  vset                 FLOAT(12, 6) NOT NULL,
  iset                 FLOAT(12, 6) NOT NULL,
  ramp_time            FLOAT(12, 6) NOT NULL,
  settle_time          FLOAT(12, 6) NOT NULL,
  ramping_steps        FLOAT(12, 6) NOT NULL,
  max_measurement_time FLOAT(12, 6) NOT NULL,
  measurement_type     VARCHAR(50)  NOT NULL,
  PRIMARY KEY (sensor_name)
);

CREATE TABLE IF NOT EXISTS date_rand (
  date  DATETIME NOT NULL,
  value INT      NOT NULL,
  PRIMARY KEY (date)
);

CREATE TABLE IF NOT EXISTS readout (
  cal_data    FLOAT(12, 6) NOT NULL,
  date        DATETIME     NOT NULL,
  board       INT          NOT NULL,
  dosimeter   INT          NOT NULL,
  vmeas       FLOAT(12, 6) NOT NULL,
  imeas       FLOAT(12, 6) NOT NULL,
  temp        FLOAT(12, 6) NOT NULL,
  sensor_name VARCHAR(50)  NOT NULL REFERENCES sensor (sensor_name),
  tag         VARCHAR(50),
  PRIMARY KEY (date, board, dosimeter)
);

CREATE TABLE IF NOT EXISTS calibration (
  sensor_name    varchar(50)  NOT NULL REFERENCES sensor (sensor_name),
  function_type  varchar(50)  NOT NULL,
  parameter_a    FLOAT(12, 6) NOT NULL,
  parameter_b    FLOAT(12, 6) NOT NULL,
  interval_start FLOAT(12, 6) NOT NULL,
  interval_end   FLOAT(12, 6) NOT NULL,
  PRIMARY KEY (sensor_name, interval_start, interval_end)
);

CREATE TABLE IF NOT EXISTS configuration (
  config_name varchar(10) NOT NULL,
  input00     INT         NOT NULL,
  input01     INT         NOT NULL,
  input02     INT         NOT NULL,
  input03     INT         NOT NULL,
  input04     INT         NOT NULL,
  input05     INT         NOT NULL,
  input06     INT         NOT NULL,
  input07     INT         NOT NULL,
  input08     INT         NOT NULL,
  input09     INT         NOT NULL,
  input010    INT         NOT NULL,
  input10     INT         NOT NULL,
  input11     INT         NOT NULL,
  input12     INT         NOT NULL,
  input13     INT         NOT NULL,
  input14     INT         NOT NULL,
  input15     INT         NOT NULL,
  input16     INT         NOT NULL,
  input17     INT         NOT NULL,
  input18     INT         NOT NULL,
  input19     INT         NOT NULL,
  input110    INT         NOT NULL,
  input20     INT         NOT NULL,
  input21     INT         NOT NULL,
  input22     INT         NOT NULL,
  input23     INT         NOT NULL,
  input24     INT         NOT NULL,
  input25     INT         NOT NULL,
  input26     INT         NOT NULL,
  input27     INT         NOT NULL,
  input28     INT         NOT NULL,
  input29     INT         NOT NULL,
  input210    INT         NOT NULL,
  input30     INT         NOT NULL,
  input31     INT         NOT NULL,
  input32     INT         NOT NULL,
  input33     INT         NOT NULL,
  input34     INT         NOT NULL,
  input35     INT         NOT NULL,
  input36     INT         NOT NULL,
  input37     INT         NOT NULL,
  input38     INT         NOT NULL,
  input39     INT         NOT NULL,
  input310    INT         NOT NULL,
  input40     INT         NOT NULL,
  input41     INT         NOT NULL,
  input42     INT         NOT NULL,
  input43     INT         NOT NULL,
  input44     INT         NOT NULL,
  input45     INT         NOT NULL,
  input46     INT         NOT NULL,
  input47     INT         NOT NULL,
  input48     INT         NOT NULL,
  input49     INT         NOT NULL,
  input410    INT         NOT NULL,
  input50     INT         NOT NULL,
  input51     INT         NOT NULL,
  input52     INT         NOT NULL,
  input53     INT         NOT NULL,
  input54     INT         NOT NULL,
  input55     INT         NOT NULL,
  input56     INT         NOT NULL,
  input57     INT         NOT NULL,
  input58     INT         NOT NULL,
  input59     INT         NOT NULL,
  input510    INT         NOT NULL,
  input60     INT         NOT NULL,
  input61     INT         NOT NULL,
  input62     INT         NOT NULL,
  input63     INT         NOT NULL,
  input64     INT         NOT NULL,
  input65     INT         NOT NULL,
  input66     INT         NOT NULL,
  input67     INT         NOT NULL,
  input68     INT         NOT NULL,
  input69     INT         NOT NULL,
  input610    INT         NOT NULL,
  input70     INT         NOT NULL,
  input71     INT         NOT NULL,
  input72     INT         NOT NULL,
  input73     INT         NOT NULL,
  input74     INT         NOT NULL,
  input75     INT         NOT NULL,
  input76     INT         NOT NULL,
  input77     INT         NOT NULL,
  input78     INT         NOT NULL,
  input79     INT         NOT NULL,
  input710    INT         NOT NULL,
  PRIMARY KEY (config_name)
);


COMMIT;

var stored_rows;
$(document).ready(function () {
    $(".datepicker-modal").css("color", "#1a237e");
    $(".datepicker-date-display").css("background-color", "#1a237e");
    $(".dropdown-content>li>a").css("color", "#1a237e");
    $('.tooltipped').tooltip();
    $("#modaldel").modal();


});

document.addEventListener('DOMContentLoaded', function () {
    $('#datefrom').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#timefrom').timepicker({
        twelveHour: false,
        defaultTime: '00:00'
    });
    $('#dateto').datepicker({
        format: 'yyyy-mm-dd'
    })
    $('#timeto').timepicker({
        twelveHour: false,
        defaultTime: '23:59'
    });

    $("#remove-def").on("click", function () {
        var arr = new Array();
        //console.log(stored_rows);
        for (var i in stored_rows) {
            var looking_for_name = "b" + stored_rows[i][0] + "-" + stored_rows[i][1];
            var elem = document.getElementById(looking_for_name);
            if (elem.checked) {
                console.log(looking_for_name + " FOUND");
                arr.push(stored_rows[i]);
            }
        }

        datefrom_val = $("#datefrom").val();
        dateto_val = $("#dateto").val();
        timefrom_val = $("#timefrom").val();
        timeto_val = $("#timeto").val();
        tag_val = $("#tag").val();
        var stringified = JSON.stringify(arr);
        var data = {
            datefrom: datefrom_val,
            dateto: dateto_val,
            timefrom: timefrom_val,
            timeto: timeto_val,
            tag: tag_val,
            board_info: stringified
        };
        console.log(data);

        $.ajax({
            type: "DELETE",
            url: 'downloads',
            data: data,
            dataType: 'json',
            error: function (xhr, error) {
                console.log("XHR: ");
                console.log(xhr);
                console.log("ERROR: " + error);
            },
            success: function (results) {
                console.log("LOADING SETTINGS...");
                console.log(results);
                if (results != "NONE") {
                    console.log("Estoy aqui");
                    var txt = "<a href=\"/download_file2\" download id='link'>";
                    $("#content").append(txt);
                    window.open($("#link").attr('href'), '_blank');
                    //$("#asdfg").click();
                    $("#link").remove();
                }
                else {

                }


            }
        });
        console.log("im here");
    });

    $.ajax({
        type: 'GET',
        url: 'downloads',
        dataType: "json",
        success: function (results) {
            console.log(results);
            if (results != "ERROR") {
                add_options_to_form(results);
            }
            else {
                $("#no_content_modal").modal({
                    dismissible: false
                });
                M.Modal.getInstance($("#no_content_modal")).open();
            }
        },
        cache: false
    });
    $("#send_form").on("click", function () {
        var arr = new Array();
        //console.log(stored_rows);
        for (var i in stored_rows) {
            var looking_for_name = "b" + stored_rows[i][0] + "-" + stored_rows[i][1];
            var elem = document.getElementById(looking_for_name);
            if (elem.checked) {
                console.log(looking_for_name + " FOUND");
                arr.push(stored_rows[i]);
            }
        }

        datefrom_val = $("#datefrom").val();
        dateto_val = $("#dateto").val();
        timefrom_val = $("#timefrom").val();
        timeto_val = $("#timeto").val();
        tag_val = $("#tag").val();
        var stringified = JSON.stringify(arr);
        var data = {
            datefrom: datefrom_val,
            dateto: dateto_val,
            timefrom: timefrom_val,
            timeto: timeto_val,
            tag: tag_val,
            board_info: stringified
        };
        console.log(data);

        $.ajax({
            type: "POST",
            url: 'downloads',
            data: data,
            dataType: 'json',
            error: function (xhr, error) {
                console.log("XHR: ");
                console.log(xhr);
                console.log("ERROR: " + error);
            },
            success: function (results) {
                console.log("LOADING SETTINGS...");
                console.log(results);
                if (results != "NONE") {
                    console.log("Estoy aqui");
                    var txt = "<a href=\"/download_file\" download id='link'>";
                    $("#content").append(txt);
                    window.open($("#link").attr('href'), '_blank');
                    //$("#asdfg").click();
                    $("#link").remove();
                }
                else {

                }


            }
        });
        console.log("im here");
    });


});


function add_options_to_form(rows) {
    stored_rows = rows;
    //console.log(stored_rows);
    var name1 = "#options_sensor1";
    var name2 = "#options_sensor2";
    var name3 = "#options_sensor3";
    var name4 = "#options_sensor4";
    var txt1 = "";
    var txt2 = "";
    var txt3 = "";
    var txt4 = "";
    for (var i in  rows) {
        var name_board = "[" + rows[i][2] + "] Board-" + rows[i][0] + "/Sensor-" + rows[i][1];
        switch (i % 4) {
            case 0:
                //console.log(1);
                txt1 += "                            <label>\n" +
                    "                                <input type=\"checkbox\" name=\"b" + rows[i][0] + "-" + rows[i][1] + "\" id=\"b" + rows[i][0] + "-" + rows[i][1] + "\"/>\n" +
                    "                                <span>" + name_board + "</span>\n" +
                    "                            </label>";
                break;
            case 1:
                //console.log(2);
                txt2 += "                            <label>\n" +
                    "                                <input type=\"checkbox\" name=\"b" + rows[i][0] + "-" + rows[i][1] + "\" id=\"b" + rows[i][0] + "-" + rows[i][1] + "\"/>\n" +
                    "                                <span>" + name_board + "</span>\n" +
                    "                            </label>";
                break;
            case 2:
                //console.log(3);
                txt3 += "                            <label>\n" +
                    "                                <input type=\"checkbox\" name=\"b" + rows[i][0] + "-" + rows[i][1] + "\" id=\"b" + rows[i][0] + "-" + rows[i][1] + "\"/>\n" +
                    "                                <span>" + name_board + "</span>\n" +
                    "                            </label>";
                break;
            case 3:
                //console.log(4);
                txt4 += "                            <label>\n" +
                    "                                <input type=\"checkbox\" name=\"b" + rows[i][0] + "-" + rows[i][1] + "\" id=\"b" + rows[i][0] + "-" + rows[i][1] + "\"/>\n" +
                    "                                <span>" + name_board + "</span>\n" +
                    "                            </label>";
                break;
        }
        //( board_num, channel, dosimeter_name)


    }

    $(name1).append(txt1);
    $(name2).append(txt2);
    $(name3).append(txt3);
    $(name4).append(txt4);

}
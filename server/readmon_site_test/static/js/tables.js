var prev;
var state;
var number_of_sensors = 0;
var sensors = null;
var table_row;
var max_sensors = 8;
var max, entries, tag;
var t_sec = 1000;
var t_min = 60 * t_sec;
var t_hour = 60 * t_min;
var t_day = 24 * t_hour;


document.addEventListener('DOMContentLoaded', function () {

    check_state();
    $('.tooltipped').tooltip();

    $("#change_state").on("click", function () {

        if (state == 0) {
            $.ajax({
                url: 'start_measurements',
                success: function (results) {
                    console.log(results);
                    if(results == "STILL RUNNING"){
                        M.toast({html: 'There is still a process ending...'});
                    }
                    if (results == "SUCCESS") {
                        state = 1 - state;
                        set_state(state);
                        console.log("STARTED")
                    }
                },
                cache: false
            });
        } else {
            $.ajax({
                url: 'stop_measurements',
                success: function (results) {
                    console.log(results);
                    if (results == "SUCCESS") {
                        state = 1 - state;
                        set_state(state);
                        console.log("STOPED");
                    }
                },
                cache: false
            });
        }

    });

    $("#save_state").on("click", function () {
        /*if (validate_minimum_times()) {
            $("#send_req").click();
        }*/
        if (validate_minimum_times()) {
            save_state_form();
        }
    });

    /*$("#lever").on("click", function () {

        $("#tag").prop('disabled', 1 - $("#lever").prop("checked"));
    });*/
    get_configuration();
});

function save_state_form() {

    $.ajax({
        url: 'save_frequency',
        type: "POST",
        data: $("#save_frequency_form").serialize(),
        dataType: 'json',
        success: function (results) {
            console.log(results);
            if (results == "ERROR") {
                M.toast({html: 'There is an error, try using numbers without decimals'});
            }
            else if (results == "OK") {
                location.reload();
            }
        },
        cache: false
    });
}

function to_millis(num, unit) {


    console.log(num + "- " + unit);
    if (unit == "Second(s)") {

        return num * t_sec;
    }
    else if (unit == "Minute(s)") {
        return num * t_min;
    }
    else if (unit == "Hour(s)") {
        return num * t_hour;
    }
    else if (unit == "Day(s)") {
        return num * t_day;
    }

}

//$('#myCheckbox').prop('checked', true); // Checks it
//$('#myCheckbox').prop('checked', false); // Unchecks it
function validate_minimum_times() {
    var sum = 0;
    for (var i = 0; i < entries.length; i++) {
        var number_name = "#frequency_" + i;
        var group_name = "group" + i;
        var number_val = $(number_name).val();
        var checked_val = $("input[name=" + group_name + "]:checked").next().text();
        val = 15000 / to_millis(number_val, checked_val);
        console.log(val);
        sum += val * entries[i][4];
    }

    console.log(sum + " : " + max);
    /*if(sum <= max){
        return true;
    }*/
    if (sum <= 1) {
        $("#error_information").hide();
        return true;
    }
    else {
        M.toast({html: 'Minimum period is ' + sum.toFixed(2) + ' times smaller'});
        var a = Math.ceil(max / 1000)
        $("#seconds_span").text("" + a);
        a = Math.ceil(a / 60);
        $("#minutes_span").text("" + a);
        a = Math.ceil(a / 60);
        $("#hours_span").text("" + a);
        a = Math.ceil(a / 24);
        $("#days_span").text("" + a);
        $("#error_information").show();
        return false;
    }
}

function generate_rows(rows) {
    console.log(rows);
    var name = "#table-body";
    var txt = "";
    max = rows[0];
    entries = rows[1];
    tag = rows[2];
    if(tag != ""){
        $("#tag").val(tag);
    }

    /*lever = rows[3];
    console.log("LEVER: " + lever);
    //console.log("TAG: " + tag);

    if (lever) {
        $("#lever").click();
    }*/
    var results = rows[1];
    for (var i in results) {
        txt += "<tr>\n" +
            "                            <td>" + results[i][1] + "</td>\n" +
            "                            <td>" + results[i][0] + "</td>\n" +
            "                            <td>\n" +
            "                                <input id=\"frequency_" + i + "\" name=\"frequency_" + i + "\" " +
            "type=\"text\" class=\"validate\" value=\"" + results[i][2] + "\">\n" +
            "                            </td>\n" +
            "                            <td>\n" +
            "                                <label>\n" +
            "                                    <input class=\"with-gap\" name=\"group" + i + "\" value=\"seconds\" type=\"radio\" ";
        if (results[i][3] == "seconds") {
            txt += "checked"
        }
        txt += "/>\n" +
            "                                    <span>Second(s)</span>\n" +
            "                                </label>\n" +
            "                                <label>\n" +
            "                                    <input class=\"with-gap\" name=\"group" + i + "\" value=\"minutes\" type=\"radio\" ";
        if (results[i][3] == "minutes") {
            txt += "checked"
        }
        txt += "/>\n" +
            "                                    <span>Minute(s)</span>\n" +
            "                                </label>\n" +
            "                                <label>\n" +
            "                                    <input class=\"with-gap\" name=\"group" + i + "\" value=\"hours\" type=\"radio\" ";
        if (results[i][3] == "hours") {
            txt += "checked"
        }
        txt += "/>\n" +
            "                                    <span>Hour(s)</span>\n" +
            "                                </label>\n" +
            "                                <label>\n" +
            "                                    <input class=\"with-gap\" name=\"group" + i + "\" value=\"days\" type=\"radio\" ";
        if (results[i][3] == "days") {
            txt += "checked"
        }
        txt += "/>\n" +
            "                                    <span>Day(s)</span>\n" +
            "                                </label>\n" +
            "                            </td>\n" +
            "                        </tr>";
        number_of_sensors++;
    }
    //console.log(txt);
    console.log("Created a new row");

    $(name).append(txt);
    return txt;
}

function get_configuration() {
    $.ajax({
        url: 'frequency_settings',
        type: "POST",
        dataType: 'json',
        success: function (results) {
            console.log(results);
            if (results != "NONE") {
                generate_rows(results);
            } else {
                $("#no_content_modal").modal({
                    dismissible: false
                });
                M.Modal.getInstance($("#no_content_modal")).open();
            }
        },
        cache: false
    });

}

function check_state() {
    $.ajax({
        url: 'is_running',
        success: function (results) {
            if (results == "true") {
                state = 1;
                set_state(state);

            } else {
                state = 0;
                set_state(state);

            }
        },
        cache: false
    });
}

function set_state(state) {
    if (state == 1) {
        $("#change_state").css('background-color', '#f44336'); //RED
        $("#change_state").children("i").text("stop");
        $("#change_state").children("span").text("Stop Measurements");
    } else {
        $("#change_state").css('background-color', '#26a69a'); //GREEN
        $("#change_state").children("i").text("play_arrow");
        $("#change_state").children("span").text("Start Measurements");

    }
}


function get_value_from_input_form(name) {
    var elem = document.getElementById(name);
    if ((elem != undefined) && (elem != null)) {
        return elem.value;
    } else {
        return "None";
    }
}

function store_configuration() {
    var arr = new Array();
    for (var i = 0; i < number_of_sensors; i++) {
        var frequency_name = "frequency_" + i;
        arr.push([])

    }
}
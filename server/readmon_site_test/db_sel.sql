SELECT * FROM ( 
	SELECT @row := @row +1 AS rownum, date, value FROM ( 
        SELECT @row :=0) r, RADMON.date_rand ) ranked  
WHERE rownum % 5 = 1 

SET @row_number = 0;

SELECT * FROM (
	SELECT *, @row := @row + 1 AS rownum FROM (
		r, RADMON.date_rand, (SELECT @row := 0) ranked
		)
) WHERE rownum % 5 = 1;
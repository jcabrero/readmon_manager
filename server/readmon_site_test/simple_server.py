from bottle import route, run, request, response, static_file, redirect
import os
#My imports
#import login
#from sensors import get_sensors, get_data
#from parallel_process import MyProcess
import settings
user_list = 0
process = MyProcess();

##############################################################################################
# FUNCTION: LOGIN
# METHOD: GET
# DESCRIPTION:  This method is executed when we login. It verifies whether there is a session
##############################################################################################
@route('/login')
def logon():
    if(login.verify_session(request, response)):
        redirect("/settings")
    return static_file('login.html', root='html/')

##############################################################################################
# FUNCTION: LOGIN
# METHOD: POST
# DESCRIPTION:  Method executed when login form is sent. Verifies the identity with database
##############################################################################################
@route('/login', method="POST")
def logon():
    if(login.authenticate_user(request.forms.get('login'), request.forms.get('password'))):
        login.create_session(response, request.forms.get('login'))
        redirect("/settings")
    return static_file('login.html', root='html/')

##############################################################################################
# FUNCTION: SETTINGS
# METHOD: GET
# DESCRIPTION:  This method loads the settings webpage
##############################################################################################
@route('/')
@route('/settings')
def setting():
    if(not login.verify_session(request, response)):
        redirect("/login")
    print("ALREADY AUTHENTICATED")
    return static_file('settings.html', root='html/')

##############################################################################################
# FUNCTION: CHARTS
# METHOD: GET
# DESCRIPTION:  This method loads the charts webpage.
##############################################################################################
@route('/charts')
def charts():
    if(not login.verify_session(request, response)):
        redirect("/login")
    print("ALREADY AUTHENTICATED")
    return static_file('charts.html', root='html/')

##############################################################################################
# FUNCTION: TABLES
# METHOD: GET
# DESCRIPTION:  This method loads the tables webpage.
##############################################################################################
@route('/tables')
def tables():
    if(not login.verify_session(request, response)):
        redirect("/login")
    print("ALREADY AUTHENTICATED")
    return static_file('tables.html', root='html/')


##############################################################################################
# FUNCTION: TABLES
# METHOD: GET
# DESCRIPTION:  This method loads the tables webpage.
##############################################################################################
@route('/try')
def trial():
    return static_file('try.html', root='html/')

##############################################################################################
# FUNCTION: TABLES
# METHOD: GET
# DESCRIPTION:  This method loads the tables webpage.
##############################################################################################
@route('/upload', method='POST')
def do_upload():
    #category = request.forms.get('category')
    upload = request.files.get('upload')
    if(upload is None):
        redirect('/settings')
    name, ext = os.path.splitext(upload.filename)
    #if ext not in ('.png', '.jpg', '.jpeg', '.txt'):
    #    return "File extension not allowed."
    #save_path = "/tmp/{category}".format(category=category)
    #if not os.path.exists(save_path):
    #    os.makedirs(save_path)
    #file_path = "{path}/{file}".format(path=save_path, file=upload.filename)
    #upload.save(file_path)
    settings.process_file(upload.file)
    redirect('/settings')
##############################################################################################
# FUNCTION: SENSORS
# METHOD: POST
# DESCRIPTION:  This method gives the information to the webpage of the existing sensors
##############################################################################################
@route('/sensors', method="POST")
def sensors():
    return get_sensors()

##############################################################################################
# FUNCTION: STORED SETTINGS
# METHOD: POST
# DESCRIPTION:  Returns the set of stored settings that the user has.
##############################################################################################
@route('/stored_settings', method="POST")
def stored_settings():
    return settings.get_configuration()


##############################################################################################
# FUNCTION: RANDOM
# METHOD: GET
# DESCRIPTION:  Returns a random coordinates (x, y, elapsed_time)
##############################################################################################
#@route('/random')
#def rnd():
#    return get_random();

##############################################################################################
# FUNCTION: GRAPHDATA
# METHOD: GET
# DESCRIPTION:  Returns a the coordinates with delay(x, y, elapsed_time)
##############################################################################################
#@route('/graphdata/')
#def graphdata():
    #return get_data();

##############################################################################################
# FUNCTION: RANDOM
# METHOD: GET
# DESCRIPTION:  Returns a random coordinates (x, y, elapsed_time)
##############################################################################################
@route('/querydata', method='GET')
def graphdata():
    my_dict = request.query.decode()
    print('datefrom',request.forms.get('datefrom'))
    print('timefrom',request.forms.get('timefrom'))
    print('dateto',request.forms.get('dateto'))
    print('timeto',request.forms.get('timeto'))
    if "date" in my_dict:
        print(my_dict["date"])
        return get_data(my_dict["date"]);
    else:
        print("NO PRESENT KEY")
        return get_data();
##############################################################################################
# FUNCTION: QUERY DATA
# METHOD: POST
# DESCRIPTION:  Returns a data from the date specified
##############################################################################################
@route('/querydata', method='POST')
def graphdata():
    my_dict = request.query.decode()
    idate = None
    edate = None 
    #datefrom = datetime.now().strftime('%Y-%m-%d') if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    datefrom = None if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    timefrom = "00:00:00" if ((request.forms.get('timefrom') is "") or (request.forms.get('timefrom') is None) or (datefrom is None)) else request.forms.get('timefrom')
    print('datefrom', datefrom)
    print('timefrom', timefrom)
    timefrom = timefrom + ":00" if len(timefrom) < 7 else timefrom;
    if(datefrom is not None):
        idate = "%s %s"%(datefrom, timefrom)
    #dateto = datetime.now().strftime('%Y-%m-%d') if request.forms.get('dateto') is "" else request.forms.get('dateto')
    dateto = None if request.forms.get('dateto') is "" else request.forms.get('dateto')
    timeto =  "23:59:00" if ((request.forms.get('timeto') is "") or (request.forms.get('timeto') is None) or (dateto is None)) else request.forms.get('timeto')
    print('dateto', dateto)
    print('timeto', timeto)
    timeto = timeto + ":00" if len(timeto) < 7 else timeto;
    samplingrate = 15000 if request.forms.get('samplingrate') is "" else request.forms.get('samplingrate')
    if(dateto is not None):
        edate = "%s %s"%(dateto, timeto)

    print("idate", idate)
    print("edate", edate)
    print('samplingrate', samplingrate)

    return get_data(idate, edate, samplingrate)

##############################################################################################
# FUNCTION: INITIAL DATA
# METHOD: POST
# DESCRIPTION:  Returns data for the live graph
##############################################################################################
@route('/initdata', method='POST')
def initdata():
    my_dict = request.query.decode()
    idate = None
    edate = None 
    #datefrom = datetime.now().strftime('%Y-%m-%d') if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    datefrom = None if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    timefrom = "00:00:00" if ((request.forms.get('timefrom') is "") or (request.forms.get('timefrom') is None) or (datefrom is None)) else request.forms.get('timefrom')
    print('datefrom', datefrom)
    print('timefrom', timefrom)
    timefrom = timefrom + ":00" if len(timefrom) < 7 else timefrom;
    if(datefrom is not None):
        idate = "%s %s"%(datefrom, timefrom)
    #dateto = datetime.now().strftime('%Y-%m-%d') if request.forms.get('dateto') is "" else request.forms.get('dateto')
    dateto = None if request.forms.get('dateto') is "" else request.forms.get('dateto')
    timeto =  "23:59:00" if ((request.forms.get('timeto') is "") or (request.forms.get('timeto') is None) or (dateto is None)) else request.forms.get('timeto')
    print('dateto', dateto)
    print('timeto', timeto)
    timeto = timeto + ":00" if len(timeto) < 7 else timeto;
    samplingrate = 15000 if request.forms.get('samplingrate') is "" else request.forms.get('samplingrate')
    if(dateto is not None):
        edate = "%s %s"%(dateto, timeto)

    print("idate", idate)
    print("edate", edate)
    print('samplingrate', samplingrate)

    return get_data(idate, edate, samplingrate)
    
##############################################################################################
# FUNCTION: START_MEASUREMENTS
# METHOD: GET
# DESCRIPTION:  This method is executed when the start measurements button is pressed.
#               It evaluates whether there is a process running and if not, it launches one.
##############################################################################################
@route('/start_measurements')
def start_measurements():
    global process
    if(process.is_running()):
        return "ALREADY RUNNING"
    process = MyProcess()
    try:
        process.start()
        return "SUCCESS"
    except (RuntimeError, TypeError, NameError) as e:
        print (e)
        return "ERROR"

##############################################################################################
# FUNCTION: STOP_MEASUREMENTS
# METHOD: GET
# DESCRIPTION:  This method when the stop measurements button is pressed. In the case that
#               there is no process running, it stops the method.
##############################################################################################
@route('/stop_measurements')
def stop_measurements():
    global process
    if(not process.is_running()):
        return "NO PROCESS RUNNING"
    try:
        process.shutdown()
        process.join()
        return "SUCCESS"
    except (RuntimeError, TypeError, NameError) as e:
        print (e)
        return "ERROR"

##############################################################################################
# FUNCTION: IS_RUNNING
# METHOD: GET
# DESCRIPTION:  This method evaluates whether there is a running process or not.
##############################################################################################
@route('/is_running')
def is_process_running():
    global process
    return "true" if process.is_running() else "false"

##############################################################################################
# FUNCTION: CSS FILES
# METHOD: GET
# DESCRIPTION:  This method returns CSS files on demand
##############################################################################################
@route('/static/css/<filename>')
def send_static(filename):
    return static_file(filename, root='static/css')

##############################################################################################
# FUNCTION: JS FILES
# METHOD: GET
# DESCRIPTION:  This method returns JS files on demand
##############################################################################################
@route('/static/js/<filename>')
def send_static(filename):
    return static_file(filename, root='static/js')

##############################################################################################
# FUNCTION: CSS FONT FILES
# METHOD: GET
# DESCRIPTION:  This method returns CSS FONT files on demand
##############################################################################################
@route('/static/css/fonts/<filename>')
def send_static(filename):
    return static_file(filename, root='static/css/fonts')
    
run(host='yunirradrh.cern.ch', port=8080, debug=True)

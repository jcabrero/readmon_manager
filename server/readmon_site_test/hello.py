import os
import configuration
#################
#               #
# CONFIGURATION #
#               #
#################
##################################
# PATH TO SERVER
path_s = configuration.path()
# IP ASSIGNED
host_s = configuration.host()
# PORT ASSIGNED
port_s = configuration.port()
# DEBUG MODE
debug_s = configuration.debug()
##################################
os.chdir(path_s)
from bottle import route, run, request, response, static_file, redirect
from datetime import datetime
import json
# My imports
import login
from sensors import get_sensors, get_data_2, update_sensor, delete_sensor, insert_sensor, insert_calibrations, \
    get_calibration_for_sensor, get_data_for_download, get_data_3, remove_data, get_tags
from parallel_process import MyProcess
import settings

user_list = 0
process = MyProcess();


##############################################################################################
# FUNCTION: LOGIN
# METHOD: GET
# DESCRIPTION:  This method is executed when we login. It verifies whether there is a session
##############################################################################################
@route('/login')
def logon():
    if (login.verify_session(request, response)):
        redirect("/settings")
    return static_file('login.html', root='html/')


##############################################################################################
# FUNCTION: LOGIN
# METHOD: POST
# DESCRIPTION:  Method executed when login form is sent. Verifies the identity with database
##############################################################################################
@route('/login', method="POST")
def logon():
    if (login.authenticate_user(request.forms.get('login'), request.forms.get('password'))):
        login.create_session(response, request.forms.get('login'))
        redirect("/settings")
    return static_file('login.html', root='html/')


##############################################################################################
# FUNCTION: SETTINGS
# METHOD: GET
# DESCRIPTION:  This method loads the settings webpage
##############################################################################################
@route('/')
@route('/settings')
def setting():
    if (not login.verify_session(request, response)):
        redirect("/login")
    print("ALREADY AUTHENTICATED")
    return static_file('settings.html', root='html/')


##############################################################################################
# FUNCTION: CHARTS
# METHOD: GET
# DESCRIPTION:  This method loads the charts webpage.
##############################################################################################
@route('/charts')
def charts():
    if (not login.verify_session(request, response)):
        redirect("/login")
    print("ALREADY AUTHENTICATED")
    return static_file('charts.html', root='html/')


##############################################################################################
# FUNCTION: TABLES
# METHOD: GET
# DESCRIPTION:  This method loads the tables webpage.
##############################################################################################
@route('/tables')
def tables():
    if (not login.verify_session(request, response)):
        redirect("/login")
    print("ALREADY AUTHENTICATED")
    return static_file('tables.html', root='html/')


##############################################################################################
# FUNCTION: CHARTS
# METHOD: GET
# DESCRIPTION:  This method loads the charts webpage.
##############################################################################################
@route('/charts2')
def charts():
    if (not login.verify_session(request, response)):
        redirect("/login")
    print("ALREADY AUTHENTICATED")
    return static_file('charts2.html', root='html/')


##############################################################################################
# FUNCTION: CHARTS
# METHOD: GET
# DESCRIPTION:  This method loads the charts webpage.
##############################################################################################
@route('/download')
def charts():
    if (not login.verify_session(request, response)):
        redirect("/login")
    print("ALREADY AUTHENTICATED")
    return static_file('download.html', root='html/')


##############################################################################################
# FUNCTION: TABLES
# METHOD: GET
# DESCRIPTION:  This method loads the tables webpage.
##############################################################################################
@route('/try')
def trial():
    return static_file('try.html', root='html/')


##############################################################################################
# FUNCTION: TABLES
# METHOD: GET
# DESCRIPTION:  This method loads the tables webpage.
##############################################################################################
@route('/upload', method='POST')
def do_upload():
    # category = request.forms.get('category')
    upload = request.files.get('upload')
    if (upload is None):
        redirect('/settings')
    name, ext = os.path.splitext(upload.filename)
    # if ext not in ('.png', '.jpg', '.jpeg', '.txt'):
    #    return "File extension not allowed."
    # save_path = "/tmp/{category}".format(category=category)
    # if not os.path.exists(save_path):
    #    os.makedirs(save_path)
    # file_path = "{path}/{file}".format(path=save_path, file=upload.filename)
    # upload.save(file_path)
    settings.process_file(upload.file)
    redirect('/settings')


##############################################################################################
# FUNCTION: STORE SETTINGS
# METHOD: GET
# DESCRIPTION:  This method loads the tables webpage.
##############################################################################################
@route('/upload', method='PUT')
def do_upload():
    input = request.forms.get('data')
    array = json.loads(input)
    return settings.update_stored_settings(array)

##############################################################################################
# FUNCTION: SENSORS
# METHOD: GET
# DESCRIPTION:  This method gives the information to the webpage of the existing sensors
##############################################################################################
@route('/sensors', method="GET")
def sensors():
    return get_sensors()


##############################################################################################
# FUNCTION: SENSORS
# METHOD: POST
# DESCRIPTION:  This method gives the information to the webpage of the existing sensors
##############################################################################################
@route('/sensors', method="POST")
def sensors():
    try:
        a = (request.forms.get("sensor_name"), float(request.forms.get("vset")), float(request.forms.get("iset")),
             float(request.forms.get("ramp_time")), float(request.forms.get("settle_time")),
             float(request.forms.get("ramping_steps")),
             float(request.forms.get("max_measurement_time")), request.forms.get("measurement_type"))
        insert_sensor(*a);
        return "OK";
    except Exception as e:
        print (e)
        return "ERROR " + str(e)


##############################################################################################
# FUNCTION: SENSORS
# METHOD: POST
# DESCRIPTION:  This method gives the information to the webpage of the existing sensors
##############################################################################################
@route('/sensors', method="PUT")
def sensors():
    a = (request.forms.get("old_name"), request.forms.get("sensor_name"), float(request.forms.get("vset")),
         float(request.forms.get("iset")),
         float(request.forms.get("ramp_time")), float(request.forms.get("settle_time")),
         float(request.forms.get("ramping_steps")),
         float(request.forms.get("max_measurement_time")), request.forms.get("measurement_type"))
    return update_sensor(*a)


##############################################################################################
# FUNCTION: SENSORS
# METHOD: POST
# DESCRIPTION:  This method gives the information to the webpage of the existing sensors
##############################################################################################
@route('/sensors', method="DELETE")
def sensors():
    if (not login.verify_session(request, response)):
        redirect("/login")
    name = request.forms.get("sensor_name")
    print (name)
    return delete_sensor(name)


##############################################################################################
# FUNCTION: FUNCTIONS
# METHOD: GET
# DESCRIPTION:  This method gives the information to the webpage of the existing sensors
##############################################################################################
@route('/functions', method="GET")
def functions():
    return settings.get_available_functions()


##############################################################################################
# FUNCTION: STORED SETTINGS
# METHOD: POST
# DESCRIPTION:  Returns the set of stored settings that the user has.
##############################################################################################
@route('/stored_settings', method="POST")
def stored_settings():
    return settings.get_configuration()


##############################################################################################
# FUNCTION: STORED SETTINGS
# METHOD: POST
# DESCRIPTION:  Returns the set of stored settings that the user has.
##############################################################################################
@route('/frequency_settings', method="POST")
def frequency_settings():
    return settings.get_configuration_for_periods()


##############################################################################################
# FUNCTION: SAVED FREQUENCY
# METHOD: POST
# DESCRIPTION:  Returns the set of stored settings that the user has.
##############################################################################################
@route('/save_frequency', method="POST")
def save_frequency():
    try:
        lst = settings.get_configuration_for_periods(1)
        if (lst is "NONE"):
            return "NONE"
        # print (lst)
        # print(request.forms.get('frequency_0'))
        tag = request.forms.get('tag')
        settings.tag(tag)
        #lever = False if request.forms.get('lever') is None else True
        #print("LEVER", lever)
        #settings.lever(lever)

        for index, elem in enumerate(lst):
            a = request.forms.get('frequency_' + str(index))
            b = request.forms.get('group' + str(index))
            # print(lst[index], elem, a , b);
            lst[index] = (lst[index][0], lst[index][1], int(a), b, lst[index][4])
        print (lst)
        # redirect('/tables')
        return json.dumps("OK")
    except Exception as e:
        print (e)
        return json.dumps("ERROR")


##############################################################################################
# FUNCTION: CALIBRATIONS
# METHOD: GET
# DESCRIPTION:  Returns the set of stored calibrations that are present on the server
##############################################################################################
@route('/calibration', method="GET")
def calibrations():
    name = request.query['sensor_name']
    print(name)
    ret = get_calibration_for_sensor(name)
    print(ret)
    return json.dumps(ret)


##############################################################################################
# FUNCTION: CALIBRATIONS
# METHOD: GET
# DESCRIPTION:  Returns the set of stored calibrations that are present on the server
##############################################################################################
@route('/calibration', method="POST")
def calibrations():
    try:
        data = request.forms.get('calibrations')
        #    print(data)
        data = json.loads(data)
        #    print(data)(sensor_name, function_type, parameter_a, parameter_b, interval_start, interval_end)
        arr = [(str(x['sensor_name']), str(x['function_type']), float(x['parameter_a']), float(x['parameter_b']),
                float(x['interval_start']), float(x['interval_end'])) for x in data]
        print arr;
        ret = insert_calibrations(arr[0][0], arr)
        return json.dumps(ret)
    except Exception as e:
        print (e)
        return json.dumps("ERROR")


##############################################################################################
# FUNCTION: CALIBRATIONS
# METHOD: GET
# DESCRIPTION:  Returns the set of stored calibrations that are present on the server
##############################################################################################
@route('/downloads', method="GET")
def downloads():
    ret = settings.get_boards_and_channels(how=1)
    return json.dumps("ERROR") if len(ret) is 0 else ret

##############################################################################################
# FUNCTION: CALIBRATIONS
# METHOD: POST
# DESCRIPTION:  Returns the set of stored calibrations that are present on the server
##############################################################################################
@route('/downloads', method="POST")
def downloads_post():
    # I would not try to read this.
    # If a date is provided, it generates a dd-mm-yy hh:mm combination.
    # Otherwise its value remains to none.

    idate = None
    edate = None
    datefrom = None if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    timefrom = "00:00:00" if ((request.forms.get('timefrom') is "") or (request.forms.get('timefrom') is None) or (
            datefrom is None)) else request.forms.get('timefrom')
    print('datefrom', datefrom)
    print('timefrom', timefrom)
    timefrom = timefrom + ":00" if len(timefrom) < 7 else timefrom;
    if (datefrom is not None):
        idate = "%s %s" % (datefrom, timefrom)
    # dateto = datetime.now().strftime('%Y-%m-%d') if request.forms.get('dateto') is "" else request.forms.get('dateto')
    dateto = None if request.forms.get('dateto') is "" else request.forms.get('dateto')
    timeto = "23:59:00" if ((request.forms.get('timeto') is "") or (request.forms.get('timeto') is None) or (
            dateto is None)) else request.forms.get('timeto')
    print('dateto', dateto)
    print('timeto', timeto)
    timeto = timeto + ":00" if len(timeto) < 7 else timeto;
    if (dateto is not None):
        edate = "%s %s" % (dateto, timeto)
    print("idate", idate)
    print("edate", edate)

    tag = None if request.forms.get('tag').strip() is "" else request.forms.get('tag')

    board_info = request.forms.get('board_info')
    board_info = json.loads(board_info)
    board_info = [(int(x[0]), int(x[1]), str(x[2])) for x in board_info]
    print(board_info)
    get_data_for_download(board_info, tag, idate, edate)
    # return get_data_2(idate, edate, samplingrate)
    return json.dumps("OK")

@route('/downloads', method="DELETE")
def downloads_post():

    if (not login.verify_session(request, response)):
        redirect("/login")
    # I would not try to read this.
    # If a date is provided, it generates a dd-mm-yy hh:mm combination.
    # Otherwise its value remains to none.

    idate = None
    edate = None
    datefrom = None if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    timefrom = "00:00:00" if ((request.forms.get('timefrom') is "") or (request.forms.get('timefrom') is None) or (
            datefrom is None)) else request.forms.get('timefrom')
    print('datefrom', datefrom)
    print('timefrom', timefrom)
    timefrom = timefrom + ":00" if len(timefrom) < 7 else timefrom;
    if (datefrom is not None):
        idate = "%s %s" % (datefrom, timefrom)
    # dateto = datetime.now().strftime('%Y-%m-%d') if request.forms.get('dateto') is "" else request.forms.get('dateto')
    dateto = None if request.forms.get('dateto') is "" else request.forms.get('dateto')
    timeto = "23:59:00" if ((request.forms.get('timeto') is "") or (request.forms.get('timeto') is None) or (
            dateto is None)) else request.forms.get('timeto')
    print('dateto', dateto)
    print('timeto', timeto)
    timeto = timeto + ":00" if len(timeto) < 7 else timeto;
    if (dateto is not None):
        edate = "%s %s" % (dateto, timeto)
    print("idate", idate)
    print("edate", edate)

    tag = None if request.forms.get('tag').strip() is "" else request.forms.get('tag')

    board_info = request.forms.get('board_info')
    board_info = json.loads(board_info)
    board_info = [(int(x[0]), int(x[1]), str(x[2])) for x in board_info]
    print(board_info)
    remove_data(board_info, tag, idate, edate)
    # return get_data_2(idate, edate, samplingrate)
    return json.dumps("OK")

##############################################################################################
# FUNCTION: RANDOM
# METHOD: GET
# DESCRIPTION:  Returns a random coordinates (x, y, elapsed_time)
##############################################################################################
@route('/download_file', method="GET")
def dowload_file():
    print("DOWNLOADING FILE...")
    filename = "temp.csv"
    return static_file(filename, root='temp_files', download=filename)

##############################################################################################
# FUNCTION: RANDOM
# METHOD: GET
# DESCRIPTION:  Returns a random coordinates (x, y, elapsed_time)
##############################################################################################
@route('/download_file2', method="GET")
def dowload_file():
    print("DOWNLOADING FILE...")
    filename = "deleted_data.csv"
    return static_file(filename, root='temp_files', download=filename)

##############################################################################################
# FUNCTION: RANDOM
# METHOD: GET
# DESCRIPTION:  Returns a random coordinates (x, y, elapsed_time)
##############################################################################################
@route('/tags', method="GET")
def dowload_file():
    return get_tags()
##############################################################################################
# FUNCTION: RANDOM
# METHOD: GET
# DESCRIPTION:  Returns a random coordinates (x, y, elapsed_time)
##############################################################################################
# @route('/random')
# def rnd():
#    return get_random();

##############################################################################################
# FUNCTION: GRAPHDATA
# METHOD: GET
# DESCRIPTION:  Returns a the coordinates with delay(x, y, elapsed_time)
##############################################################################################
# @route('/graphdata/')
# def graphdata():
# return get_data();

##############################################################################################
# FUNCTION: RANDOM
# METHOD: GET
# DESCRIPTION:  Returns a random coordinates (x, y, elapsed_time)
##############################################################################################
@route('/querydata', method='GET')
def graphdata():
    my_dict = request.query.decode()
    print('datefrom', request.forms.get('datefrom'))
    print('timefrom', request.forms.get('timefrom'))
    print('dateto', request.forms.get('dateto'))
    print('timeto', request.forms.get('timeto'))
    if "date" in my_dict:
        print(my_dict["date"])
        return get_data_2(my_dict["date"]);
    else:
        print("NO PRESENT KEY")
        return get_data_2();


##############################################################################################
# FUNCTION: QUERY DATA
# METHOD: POST
# DESCRIPTION:  Returns a data from the date specified
##############################################################################################
@route('/querydata', method='POST')
def graphdata():
    my_dict = request.query.decode()
    idate = None
    edate = None
    # datefrom = datetime.now().strftime('%Y-%m-%d') if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    datefrom = None if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    timefrom = "00:00:00" if ((request.forms.get('timefrom') is "") or (request.forms.get('timefrom') is None) or (
            datefrom is None)) else request.forms.get('timefrom')
    print('datefrom', datefrom)
    print('timefrom', timefrom)
    timefrom = timefrom + ":00" if len(timefrom) < 7 else timefrom;
    if (datefrom is not None):
        idate = "%s %s" % (datefrom, timefrom)
    # dateto = datetime.now().strftime('%Y-%m-%d') if request.forms.get('dateto') is "" else request.forms.get('dateto')
    dateto = None if request.forms.get('dateto') is "" else request.forms.get('dateto')
    timeto = "23:59:00" if ((request.forms.get('timeto') is "") or (request.forms.get('timeto') is None) or (
            dateto is None)) else request.forms.get('timeto')
    print('dateto', dateto)
    print('timeto', timeto)
    timeto = timeto + ":00" if len(timeto) < 7 else timeto;
    samplingrate = 15000 if request.forms.get('samplingrate') is "" else request.forms.get('samplingrate')
    if (dateto is not None):
        edate = "%s %s" % (dateto, timeto)
    tag = request.forms.get('tag')
    tag = None if (tag is None) or (tag.strip() is "") else request.forms.get('tag')
    print("idate", idate)
    print("edate", edate)
    print('samplingrate', samplingrate)

    return get_data_2(idate, edate, samplingrate, tag)

##############################################################################################
# FUNCTION: QUERY DATA
# METHOD: POST
# DESCRIPTION:  Returns a data from the date specified
##############################################################################################
@route('/querydata2', method='POST')
def graphdata():
    my_dict = request.query.decode()
    idate = None
    edate = None
    # datefrom = datetime.now().strftime('%Y-%m-%d') if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    datefrom = None if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    timefrom = "00:00:00" if ((request.forms.get('timefrom') is "") or (request.forms.get('timefrom') is None) or (
            datefrom is None)) else request.forms.get('timefrom')
    print('datefrom', datefrom)
    print('timefrom', timefrom)
    timefrom = timefrom + ":00" if len(timefrom) < 7 else timefrom;
    if (datefrom is not None):
        idate = "%s %s" % (datefrom, timefrom)
    # dateto = datetime.now().strftime('%Y-%m-%d') if request.forms.get('dateto') is "" else request.forms.get('dateto')
    dateto = None if request.forms.get('dateto') is "" else request.forms.get('dateto')
    timeto = "23:59:00" if ((request.forms.get('timeto') is "") or (request.forms.get('timeto') is None) or (
            dateto is None)) else request.forms.get('timeto')
    print('dateto', dateto)
    print('timeto', timeto)
    timeto = timeto + ":00" if len(timeto) < 7 else timeto;
    samplingrate = 15000 if request.forms.get('samplingrate') is "" else request.forms.get('samplingrate')
    if (dateto is not None):
        edate = "%s %s" % (dateto, timeto)
    tag = request.forms.get('tag')
    tag = None if (tag is None) or (tag.strip() is "") else request.forms.get('tag')
    print("idate", idate)
    print("edate", edate)
    print('samplingrate', samplingrate)

    return get_data_3(idate, edate, samplingrate, tag)
##############################################################################################
# FUNCTION: INITIAL DATA
# METHOD: POST
# DESCRIPTION:  Returns data for the live graph
##############################################################################################
@route('/initdata', method='POST')
def initdata():
    my_dict = request.query.decode()
    idate = None
    edate = None
    # datefrom = datetime.now().strftime('%Y-%m-%d') if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    datefrom = None if request.forms.get('datefrom') is "" else request.forms.get('datefrom')
    timefrom = "00:00:00" if ((request.forms.get('timefrom') is "") or (request.forms.get('timefrom') is None) or (
            datefrom is None)) else request.forms.get('timefrom')
    print('datefrom', datefrom)
    print('timefrom', timefrom)
    timefrom = timefrom + ":00" if len(timefrom) < 7 else timefrom;
    if (datefrom is not None):
        idate = "%s %s" % (datefrom, timefrom)
    # dateto = datetime.now().strftime('%Y-%m-%d') if request.forms.get('dateto') is "" else request.forms.get('dateto')
    dateto = None if request.forms.get('dateto') is "" else request.forms.get('dateto')
    timeto = "23:59:00" if ((request.forms.get('timeto') is "") or (request.forms.get('timeto') is None) or (
            dateto is None)) else request.forms.get('timeto')
    print('dateto', dateto)
    print('timeto', timeto)
    timeto = timeto + ":00" if len(timeto) < 7 else timeto;
    samplingrate = 15000 if request.forms.get('samplingrate') is "" else request.forms.get('samplingrate')
    if (dateto is not None):
        edate = "%s %s" % (dateto, timeto)
    tag = request.forms.get('tag')
    tag = None if (tag is None) or (tag.strip() is "") else request.forms.get('tag')
    print("idate", idate)
    print("edate", edate)
    print('samplingrate', samplingrate)

    return get_data(idate, edate, samplingrate, tag)


##############################################################################################
# FUNCTION: START_MEASUREMENTS
# METHOD: GET
# DESCRIPTION:  This method is executed when the start measurements button is pressed.
#               It evaluates whether there is a process running and if not, it launches one.
##############################################################################################
@route('/start_measurements')
def start_measurements():
    global process
    if (process.is_running()):
        return "ALREADY RUNNING"
    if (process.is_background_running()):
        return "STILL RUNNING"
    tag = "NULL" if settings.tag() == "" else settings.tag()
    process = MyProcess(settings.get_tasks_for_scheduler(), tag)
    try:
        process.start()
        return "SUCCESS"
    except (RuntimeError, TypeError, NameError) as e:
        print (e)
        return "ERROR"


##############################################################################################
# FUNCTION: STOP_MEASUREMENTS
# METHOD: GET
# DESCRIPTION:  This method when the stop measurements button is pressed. In the case that
#               there is no process running, it stops the method.
##############################################################################################
@route('/stop_measurements')
def stop_measurements():
    global process
    if (not process.is_running()):
        return "NO PROCESS RUNNING"
    try:
        process.shutdown()
        # process.join()
        return "SUCCESS"
    except (RuntimeError, TypeError, NameError) as e:
        print (e)
        return "ERROR"


##############################################################################################
# FUNCTION: IS_RUNNING
# METHOD: GET
# DESCRIPTION:  This method evaluates whether there is a running process or not.
##############################################################################################
@route('/is_running')
def is_process_running():
    global process
    return "true" if process.is_running() else "false"


##############################################################################################
# FUNCTION: CSS FILES
# METHOD: GET
# DESCRIPTION:  This method returns CSS files on demand
##############################################################################################
@route('/static/css/<filename>')
def send_static(filename):
    return static_file(filename, root='static/css')


##############################################################################################
# FUNCTION: JS FILES
# METHOD: GET
# DESCRIPTION:  This method returns JS files on demand
##############################################################################################
@route('/static/js/<filename>')
def send_static(filename):
    return static_file(filename, root='static/js')


##############################################################################################
# FUNCTION: CSS FONT FILES
# METHOD: GET
# DESCRIPTION:  This method returns CSS FONT files on demand
##############################################################################################
@route('/static/css/fonts/<filename>')
def send_static(filename):
    return static_file(filename, root='static/css/fonts')


run(host=host_s, port=port_s, debug=debug_s)
# run(host='10.211.55.4', port=8080, debug=True)

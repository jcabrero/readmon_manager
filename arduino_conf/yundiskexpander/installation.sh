#!/bin/sh
execute_command(){
    #ARG1 Command
    #ARG2 value
    #ARG3 Success
    #ARG4 Error
    #echo $*
    xd=`eval $1`
    if [ "$xd" == "$2" ]
    then
        echo "$3"
    else
        echo "$4"
        exit 1
    fi
}


execute_command1(){
    #ARG1 Command
    #ARG2 value
    #ARG3 Success
    #ARG4 Error
    #echo $*
    xd=`eval $1`
    #echo $xd
    if [ "$xd" -gt "$2" ]
    then
        echo "$3"
    else
        echo "$4"
        exit 1
    fi
}

execute_command_success(){
    #ARG1 Command
    #ARG2 value
    #ARG3 Success
    #ARG4 Error
    #echo $*
    xd=`eval $1`
    if [ "$?" == "$2" ]
    then
        echo "$3"
    else
        echo "$4"
        exit 1
    fi
}
execute_command_no_out(){
    #ARG1 Command
    #echo $*
    xd=`eval $1`

}
unmount_sd(){
    execute_command_no_out "umount /dev/sda?"
    execute_command_no_out "rm -rf /mnt/sda?"
}

#CHECK EVERYTHING IS READY TO EXPAND
execute_command "mount | grep ^/dev/sda | grep 'on /overlay'"  ""  "MicroSD Card without Overlay - OK" "Micro SD card is already used as additional Arduino Yun disk space. Nothing to do."
execute_command1 "df / | awk '/rootfs/ {print \$4}'" 1000  "Arduino has enough space - OK" "You don't have enough disk space to install the utility software. You need to free at least 1MB of Flash memory."
execute_command_success "ls /mnt/sda1" 0 "SD Card Available - OK" "The micro SD card is not available"
#INSTALL SOFTWARE
echo "Updating software list..."
execute_command_success "opkg update" 0 "Software list updated. Installing software (this will take a while)..." "err. with opkg, check internet connection"
execute_command_success "opkg install e2fsprogs mkdosfs fdisk rsync" 0 "e2fsprogs mkdosfs fdisk rsync installed" "err. with opkg, could not download packages"
#PARTITIONING AND FORMATTING
unmount_sd
execute_command_no_out "dd if=/dev/zero of=/dev/sda bs=4096 count=10"
    #FIRST PARTITION
execute_command_no_out "(echo n; echo p; echo 1; echo; echo +8192M; echo w) | fdisk /dev/sda"
unmount_sd
    #SECOND PARTITION
execute_command_no_out "(echo n; echo p; echo 2; echo; echo; echo w) | fdisk /dev/sda"
unmount_sd
    #FIRST PARTITION TO FAT32
execute_command_no_out "(echo t; echo 1; echo c; echo w) | fdisk /dev/sda"
unmount_sd
sleep 5
unmount_sd
execute_command_success "mkfs.vfat /dev/sda1" 0 "Micro SD card correctly partitioned" "err. formatting to FAT32"
sleep 1
execute_command_success "mkfs.ext4 /dev/sda2" 0 "Micro SD card correctly partitioned" "formatting to EXT4"
#CREATE ARDUINO FOLDER
echo "Creating 'arduino' folder structure..."
execute_command_no_out "mkdir -p /mnt/sda1"
execute_command_no_out "mount /dev/sda1 /mnt/sda1"
execute_command_no_out "mkdir -p /mnt/sda1/arduino/www"
unmount_sd
#COPY SYSTEM FILES FROM YUN TO SD
echo "Copying files from Arduino Yun flash to micro SD card..."
execute_command_no_out "mkdir -p /mnt/sda2"
execute_command_no_out "mount /dev/sda2 /mnt/sda2"
execute_command_no_out "rsync -a --exclude=/mnt/ --exclude=/www/sd /overlay/ /mnt/sda2/"
unmount_sd
#Enabling Ext Root
echo "Enabling micro SD as additional disk space... "
execute_command_no_out "uci add fstab mount"
execute_command_no_out "uci set fstab.@mount[0].target=/overlay"
execute_command_no_out "uci set fstab.@mount[0].device=/dev/sda2"
execute_command_no_out "uci set fstab.@mount[0].fstype=ext4"
execute_command_no_out "uci set fstab.@mount[0].enabled=1"
execute_command_no_out "uci set fstab.@mount[0].enabled_fsck=0"
execute_command_no_out "uci set fstab.@mount[0].options=rw,sync,noatime,nodiratime"
execute_command_no_out "uci commit"
echo "enabled"
echo "We are done! Yeah! Now press the YUN RST button to apply the changes."
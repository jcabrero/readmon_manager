-- DB CREATION SCRIPT
CREATE DATABASE IF NOT EXISTS `RADMON`;
USE RADMON;
-- CHECK THE ONLY DATABASE ACCESSIBLE IS THE ONE WE GRANTED PRIVILEGES TO
-- SHOW DATABASES;

CREATE TABLE IF NOT EXISTS user (
  username varchar(50) NOT NULL,
  password varchar(50) NOT NULL,
  PRIMARY KEY (username)
);

CREATE TABLE IF NOT EXISTS sensor (
  sensor_name          varchar(50)  NOT NULL,
  vset                 FLOAT(12, 6) NOT NULL,
  iset                 FLOAT(12, 6) NOT NULL,
  ramp_time            FLOAT(12, 6) NOT NULL,
  settle_time          FLOAT(12, 6) NOT NULL,
  ramping_steps        FLOAT(12, 6) NOT NULL,
  max_measurement_time FLOAT(12, 6) NOT NULL,
  measurement_type     VARCHAR(50)  NOT NULL,
  PRIMARY KEY (sensor_name)
);

CREATE TABLE IF NOT EXISTS date_rand (
  date  DATETIME NOT NULL,
  value INT      NOT NULL,
  PRIMARY KEY (date)
);

CREATE TABLE IF NOT EXISTS readout (
  cal_data    FLOAT(12, 6) NOT NULL,
  date        DATETIME     NOT NULL,
  board       INT          NOT NULL,
  dosimeter   INT          NOT NULL,
  vmeas       FLOAT(12, 6) NOT NULL,
  imeas       FLOAT(12, 6) NOT NULL,
  temp        FLOAT(12, 6) NOT NULL,
  sensor_name VARCHAR(50)  NOT NULL REFERENCES sensor (sensor_name),
  tag         VARCHAR(50) NOT NULL,
  PRIMARY KEY (date, board, dosimeter)
);

CREATE TABLE IF NOT EXISTS calibration (
  sensor_name    varchar(50)  NOT NULL REFERENCES sensor (sensor_name),
  function_type  varchar(50)  NOT NULL,
  parameter_a    FLOAT(12, 6) NOT NULL,
  parameter_b    FLOAT(12, 6) NOT NULL,
  interval_start FLOAT(12, 6) NOT NULL,
  interval_end   FLOAT(12, 6) NOT NULL,
  PRIMARY KEY (sensor_name, interval_start, interval_end)
);


COMMIT;

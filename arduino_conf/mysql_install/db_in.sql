USE RADMON;

#INSERT into user(username, password) values ('jcabrero', PASSWORD('passwd'));
INSERT into user(username, password) values ('user', PASSWORD('password'));

INSERT into sensor(sensor_name, vset, iset, ramp_time, settle_time, ramping_steps, max_measurement_time, measurement_type)
values ('LBSD Si - 1', 20.000000 ,1.000000, 100, 1000, 10, 2000, "Fluence" );

INSERT into sensor(sensor_name, vset, iset, ramp_time, settle_time, ramping_steps, max_measurement_time, measurement_type)
values ('LBSD Si - 2', 20.000000 ,1.000000, 100, 1000, 10, 2000, "Dose" );

INSERT into sensor(sensor_name, vset, iset, ramp_time, settle_time, ramping_steps, max_measurement_time, measurement_type)
values ('BPW', 20.000000 ,1.000000, 100, 1000, 10, 2000, "Fluence" );

INSERT into sensor(sensor_name, vset, iset, ramp_time, settle_time, ramping_steps, max_measurement_time, measurement_type)
values ('REM', 20.000000 ,1.000000, 100, 1000, 10, 2000, "Dose" );

INSERT into sensor(sensor_name, vset, iset, ramp_time, settle_time, ramping_steps, max_measurement_time, measurement_type)
values ('LAAS', 20.000000 ,1.000000, 100, 1000, 10, 2000, "Fluence" );

INSERT into sensor(sensor_name, vset, iset, ramp_time, settle_time, ramping_steps, max_measurement_time, measurement_type)
values ('NTC', 20.000000 ,1.000000, 100, 1000, 10, 2000, "Temperature" );

INSERT into sensor(sensor_name, vset, iset, ramp_time, settle_time, ramping_steps, max_measurement_time, measurement_type)
values ('TestSensor', 20.000000 ,1.000000, 100, 1000, 10, 2000, "Fluence" );

COMMIT;


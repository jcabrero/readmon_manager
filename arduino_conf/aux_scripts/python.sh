#!/bin/sh
#author: Jose Cabrero Holgueras
#mail: jose.cabrero@alumnos.uc3m.es

execute_command_success(){
    #ARG1 Command
    #ARG2 value
    #ARG3 Success
    #ARG4 Error
    #echo $*
    xd=`eval $1`
    if [ "$?" == "$2" ]
    then
        echo "$3"
    else
        echo "$4"
        exit 1
    fi
}
execute_command_no_out(){
    #ARG1 Command
    #echo $*
    xd=`eval $1`

}

DIRECTORY=`pwd`

echo "Updating software list..."
execute_command_success "opkg update" 0 "Software list updated. Installing software (this will take a while)..." "err. with opkg, check internet connection"
execute_command_success "opkg install python python-json python-mysql" 0 "python python-json python-mysql installed" "err. with opkg, could not download packages"


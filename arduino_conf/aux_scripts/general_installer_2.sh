#!/bin/sh
#author: Jose Cabrero Holgueras
#mail: jose.cabrero@alumnos.uc3m.es
DIRECTORY=`pwd`
execute_command_no_out(){
    #ARG1 Command
    #echo $*
    xd=`eval $1`

}
#chmod 755 mysql.sh
#chmod 755 yundiskexpander.sh
#chmod 755 general_installer_2.sh
#cd ..
#mv $DIRECTORY/../arduino_config /root/S
#
#echo ">>>EXECUTING Arduino Yun Disk Expander..."
#execute_command_no_out "$DIRECTORY/yundiskexpander.sh"
#
#cp $DIRECTORY/config/init.d/continue_installation /etc/init.d
#chmod 755 /etc/init.d/continue_installation
#/etc/init.d/continue_installation enable
#
#reboot

chmod 755 mysql.sh
chmod 755 python.sh

#We remove the second installation script
/etc/init.d/continue_installation disable
rm /etc/init.d/continue_installation

echo ">>>EXECUTING MySQL Installation..."
execute_command_no_out "$DIRECTORY/mysql.sh"

echo ">>>EXECUTING Python Installation..."
execute_command_no_out "$DIRECTORY/python.sh"

echo ">>>Enabling server"

cp $DIRECTORY/config/init.d/readmon_manager /etc/init.d
chmod 755 /etc/init.d/readmon_manager
/etc/init.d/readmon_manager enable
echo "Rebooting..."
sleep 5
reboot
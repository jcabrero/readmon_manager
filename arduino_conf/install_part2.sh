#!/bin/sh
DIRECTORY=`pwd`
execute_command(){
    #ARG1 Command
    #ARG2 value
    #ARG3 Success
    #ARG4 Error
    #echo $*
    xd=`eval $1`
    if [ "$xd" == "$2" ]
    then
        echo "$3"
    else
        echo "$4"
        exit 1
    fi
}


execute_command1(){
    #ARG1 Command
    #ARG2 value
    #ARG3 Success
    #ARG4 Error
    #echo $*
    xd=`eval $1`
    #echo $xd
    if [ "$xd" -gt "$2" ]
    then
        echo "$3"
    else
        echo "$4"
        exit 1
    fi
}

execute_command_success(){
    #ARG1 Command
    #ARG2 value
    #ARG3 Success
    #ARG4 Error
    #echo $*
    xd=`eval $1`
    if [ "$?" == "$2" ]
    then
        echo "$3"
    else
        echo "$4"
        exit 1
    fi
}
execute_command_no_out(){
    #ARG1 Command
    #echo $*
    xd=`eval $1`

}
##
## MY SQL
##
echo "Updating software list..."
execute_command_success "opkg update" 0 "Software list updated. Installing software (this will take a while)..." "err. with opkg, check internet connection"
execute_command_success "opkg install mysql-server" 0 "mysql-server installed" "err. with opkg, could not download packages"

echo "Creating directories..."
execute_command_no_out "mkdir -p /mnt/data/mysql"
execute_command_no_out "mkdir -p /mnt/data/tmp"

echo "Installing mysql..."
execute_command_no_out "mysql_install_db --force"
echo "Installation succesfull. Initializing database..."
/usr/bin/mysqld --skip-grant-tables --skip-networking &
echo "Initialized. Making first configuration of database..."
execute_command_no_out "mysql -uroot -e'source $DIRECTORY/mysql_install/setup_db.sql'"

echo "Creating database service..."
/etc/init.d/mysqld enable
/etc/init.d/mysqld start

echo "Generating RADMON database..."
chmod 755 $DIRECTORY/mysql_install/db.sh
execute_command_no_out "$DIRECTORY/mysql_install/db.sh all"
##
## PYTHON
##

echo "Updating software list..."
execute_command_success "opkg update" 0 "Software list updated. Installing software (this will take a while)..." "err. with opkg, check internet connection"
execute_command_success "opkg install python python-json python-mysql" 0 "python python-json python-mysql installed" "err. with opkg, could not download packages"

##
## INITIAL PROCESS
##

echo "Moving uhttpd server to port 8000"
mv /etc/config/uhttpd /etc/config/.uhttpd.old.conf
cp $DIRECTORY/config/uhttpd /etc/config/

echo "Setting up server initialization"

cp $DIRECTORY/config/readmon_manager /etc/init.d/
chmod 755 /etc/init.d/readmon_manager
/etc/init.d/readmon_manager enable
echo "Rebooting..."
sleep 5
reboot